# Master (will become release 2.10)

- ...

## Deprecations and removals

- The method `assembleBasisInterpolationMatrix` has been removed.  It only worked
  for old `dune-fufem`-style function space bases.

- The class `AmiraMeshBasisWriter` has been removed.  It relied on AmiraMesh support
  in `dune-grid`, which has been removed before the 2.9 release.

- The class `VTKBasisWriter` has been removed. It only worked
  for old `dune-fufem`-style function space bases.


# 2.9 Release

- Various improvements to the `MappedMatrix` class
    - `MappedMatrix` objects can now be printed using `printmatrix` (from `dune-istl`).
    - Iteration over rows is now implemented.  This implies that range-based `for`-loops
      over rows also work.


# 2.8 Release

- constructBoundaryDofs:
    - Small interface change in the template parameters: drop `blocksize` and replace it by `BitSetVector`
    - The method can now handle generic `dune-functions` basis types, as long as we have consistency in the data types

- assembleGlobalBasisTransferMatrix:
    - Support for all bases in `dune-functions` compatible form added

- `istlMatrixBackend` can now be used with `MultiTypeBlockMatrix`

- The class `DuneFunctionsLocalMassAssembler` has been renamed to `MassAssembler`,
  moved into the namespace `Dune::Fufem`, and to the file `massassembler.hh`.
  The old class is still there, but it is deprecated now.
