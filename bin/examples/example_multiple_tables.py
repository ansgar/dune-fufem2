import sys

sys.path.insert(0, '..')

from regexptools import *
from iteratortools import *


# regexp for empty line
empty_line = regexpMatchFunction('^$')

# regexp for nonempty dashed line
dashed_line = regexpMatchFunction('^-+$')

# regexp for nonempty line of numbers
num_line = regexpMatchFunction('^ *'+NumericRegexp.num+'( +'+NumericRegexp.num+')* *$')

# regexp for nonempty line of numbers
nonnum_line = regexpInverseMatchFunction('^ *'+NumericRegexp.num+'( +'+NumericRegexp.num+')* *$')

# regexp for head of table
head_line = regexpMatchFunction('^ iter')
end_line = regexpMatchFunction('^G')

infile = file(sys.argv[1], 'r')
if (len(sys.argv)>2):
    outfilename = sys.argv[2]
else:
    outfilename = 'example_multiple_table_%03d.out'


# using a file iterator this method can also handle fifos propperly
# using a BufferedIterator file(sys.argv[1], 'r') we can go back
# this is needed by the end() method
it = BufferedIterator(FileIterator(infile))

i = 0
while not(it.end()):
    # iterate until head_line found
    Iterate.toEnd(UntilIterator(it,head_line))

    # iterate until num_line found, excluding numline
    Iterate.toEnd(UntilIterator(it,num_line,False))

    if not(it.end()):
        # iterator until nonnum_line found, filtering only matching num_line
        fit = FilterIterator(UntilIterator(it,nonnum_line), num_line)

        # write this matching lines to blocknum
        outfile = file(outfilename % i, 'w')
        for l in fit:
            outfile.write(l)
            outfile.flush()
            print l.rstrip()
        outfile.close()
        i += 1

infile.close()

