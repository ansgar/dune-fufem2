import sys

sys.path.insert(0, '..')

from regexptools import *
from iteratortools import *


# regexp for empty line
empty_line = regexpMatchFunction('^$')

# regexp for nonempty dashed line
dashed_line = regexpMatchFunction('^-+$')

# regexp for nonempty line of numbers
num_line = regexpMatchFunction('^ *'+NumericRegexp.num+'( +'+NumericRegexp.num+')* *$')

# regexp for nonempty line of numbers
nonnum_line = regexpInverseMatchFunction('^ *'+NumericRegexp.num+'( +'+NumericRegexp.num+')* *$')

# regexp for head of table
head_line = regexpMatchFunction('^ *act')
end_line = regexpMatchFunction('^G')

infile = file(sys.argv[1], 'r')
#outfile = file(sys.argv[2], 'w')


# using a BufferedIterator file(sys.argv[1], 'r') we can go back
# this is needed by the end() method
it = BufferedIterator(infile)

while not(it.end()):
    # iterate until head_line found
    Iterate.toEnd(UntilIterator(it,head_line))

    # iterate until num_line found, excluding numline
    Iterate.toEnd(UntilIterator(it,num_line, False))

    # iterator until nonnum_line found, filtering only matching num_line
    fit = FilterIterator(UntilIterator(it,nonnum_line), num_line)

    for l in fit:
        print l.rstrip()
#        outfile.write(l)
#        outfile.flush()

infile.close()
#outfile.close()
