import sys


class Tree(list):
    def printHierarchic(self,prefix='    ',level=0,maxlevel=100):
        for child in self:
            if isinstance(child, Tree):
                if (level<maxlevel):
                    child.printHierarchic(prefix,level+1,maxlevel)
            else:
                for i in range(0,level):
                    sys.stdout.write(prefix)
                sys.stdout.write(child.strip()+'\n')




class TreeGenerator:

    def __init__(self):
        self.tree = Tree()
        self.hierarchy = [self.tree]

    def feed(self,levelitem):
        item = levelitem[0]
        level = levelitem[1]

        # grow hierarchy to current level if neccessary
        while len(self.hierarchy)-1 < level:
            newlevel = Tree()
            self.hierarchy[-1].append(newlevel)
            self.hierarchy.append(newlevel)

        # shrink hierarchy to current level if neccessary
        while len(self.hierarchy)-1 > level:
            self.hierarchy.pop()

        self.hierarchy[-1].append(item)

