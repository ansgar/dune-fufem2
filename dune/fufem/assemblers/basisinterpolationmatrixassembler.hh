// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH

#include <type_traits>
#include <typeinfo>
#include <unordered_set>

#include <dune/common/bitsetvector.hh>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/fufem/functions/cachedcomponentwrapper.hh>
#include <dune/fufem/assemblers/istlbackend.hh>

#include <dune/functions/functionspacebases/basistags.hh>

#include <dune/typetree/visitor.hh>
#include <dune/typetree/pairtraversal.hh>


/** \brief Wrapper that extracts a single local basis function. */
template<class LocalBasis, class Base>
class LocalBasisComponentWrapper :
    public CachedComponentWrapper<
        LocalBasisComponentWrapper<LocalBasis, Base>,
        std::vector<typename LocalBasis::Traits::RangeType>,
        Base>
{
  public:
    typedef std::vector<typename LocalBasis::Traits::RangeType> AllRangeType;

  private:
    typedef CachedComponentWrapper<LocalBasisComponentWrapper<LocalBasis, Base>, AllRangeType, Base> BaseClass;

  public:
    typedef typename Base::DomainType DomainType;
    typedef typename Base::RangeType RangeType;

        LocalBasisComponentWrapper(const LocalBasis& localBasis, int comp) :
            BaseClass(comp),
            localBasis_(localBasis)
        {}

        void evaluateAll(const DomainType& x, AllRangeType& y) const
        {
            localBasis_.evaluateFunction(x, y);
        }

    protected:
        const LocalBasis& localBasis_;
};


/** \brief Wrapper for evaluating local functions in another geometry */
template<class Function, class Geometry>
class OtherGeometryWrapper
{

public:

  OtherGeometryWrapper(const Function& function, const Geometry& geometry)
  : function_(function)
  , geometry_(geometry)
  {}

  // forward the evaluation into the other geometry
  template<class X>
  auto operator()(const X& x) const
  {
    return function_(geometry_.global(x));
  }



private:

  const Function function_;
  const Geometry geometry_;
};


/** \brief Wrapper chaining sequencial father geometries */
template<class GeometryContainer>
class GeometryInFathers
{

public:

  GeometryInFathers(const GeometryContainer& geometryContainer)
  : geometryContainer_(geometryContainer)
  {}

  // forward the global() method to all single geometries
  template<class X>
  auto global(const X& x) const
  {
    auto y = x;
    for (const auto& g : geometryContainer_)
    {
      y = g.global(y);
    }
    return y;
  }

private:

  const GeometryContainer geometryContainer_;
};



namespace Impl{


/** \brief Visitor for interpolating the local basis of a leaf node by a fine local basis
 *
 * The LeafNodeInterpolationVisitor traverses the local tree of the coarse basis at an
 * element bound to the coarse localView. At every leaf node the local basis is extracted and
 * interpolated by the corresponding local basis of the fine element (bound to the fine localView).
 *
 * This works for all dune-functions compatible local basis trees.
 *
 * \todo In case of powerNodes we could recycle the interpolation for all child nodes to save
 *       some assembly time.
 */

template <class CLV, class FLV, class G, class B, class M, class I>
class LeafNodeInterpolationVisitor
  : public Dune::TypeTree::TreePairVisitor
  , public Dune::TypeTree::DynamicTraversal
{
public:

  using CoarseLocalView = CLV;
  using FineLocalView = FLV;
  using Geometry = G;
  using BitSet = B;
  using Matrix = M;
  using IndexSet = I;

  /** \brief Constructor with all necessary context information
   *
   * \param [in] clv (coarseLocalView) coarse element information and localBasis
   * \param [in] flv (fineLocalView) fine element information and localBasis
   * \param [in] g (geometry) mapping fine -> coarse
   * \param [in] p (processed) set of already processed dof's
   * \param [in] t (tolerance) threshold for considering an interpolated value as zero
   * \param [out] m (matrix) resulting tranferoperator matrix (coarse basis) -> (fine basis) wrapped in ISTLBackend
   * \param [out] i (indices) index set of the matrix
   * \param [in] e (exportIndices) boolean switch: true  = add indices to the index set
   *                                               false = increase the matrix entries
   */
  LeafNodeInterpolationVisitor(const CLV& clv, const FLV& flv, const G& g, const B& p, double t, M& m, I& i, bool e)
  : coarseLocalView_(clv)
  , fineLocalView_(flv)
  , geometry_(g)
  , processed_(p)
  , tolerance_(t)
  , matrix_(m)
  , indices_(i)
  , exportIndices_(e)
  {}

  template<class CoarseNode, class FineNode, class TreePath>
  void leaf(CoarseNode& coarseNode, FineNode& fineNode, TreePath treePath)
  {
    const auto& coarseFiniteElement = coarseNode.finiteElement();
    const auto& fineFiniteElement   = fineNode.finiteElement();

    const auto& coarseLocalBasis = coarseFiniteElement.localBasis();
    const auto& fineLocalBasis   = fineFiniteElement.localBasis();

    // Hack: The RangeFieldType is the best guess of a suitable type for coefficients we have here
    using CoefficientType = typename std::decay_t<decltype(coarseLocalBasis)>::Traits::RangeFieldType;

    // interpolated coefficients for the fine local basis
    std::vector<CoefficientType> coefficients(fineLocalBasis.size());

    using CoarseFEType = std::decay_t<decltype(coarseFiniteElement)>;
    using FunctionTraits = typename CoarseFEType::Traits::LocalBasisType::Traits;
    using LocalBasisWrapper = LocalBasisComponentWrapper<typename CoarseFEType::Traits::LocalBasisType, FunctionTraits>;

    LocalBasisWrapper basisFctj(coarseLocalBasis,0);

    for (size_t j=0; j<coarseLocalBasis.size(); j++)
    {
      // wrap each local basis function as a local function.
      basisFctj.setIndex(j);

      // transform the local fine function to a local coarse function
      const auto baseFctj = OtherGeometryWrapper(basisFctj, geometry_);

      // Interpolate j^th base function by the fine basis
      fineFiniteElement.localInterpolation().interpolate(baseFctj, coefficients);

      // get the matrix col index
      const auto& globalCoarseIndex = coarseLocalView_.index( coarseNode.localIndex( j ) );

      // set the matrix indices
      for (size_t i=0; i<fineLocalBasis.size(); i++)
      {
        // some values may be practically zero -- no need to store those
        if ( std::abs(coefficients[i]) < tolerance_ )
          continue;

        // get the matrix row index
        const auto& globalFineIndex = fineLocalView_.index( fineNode.localIndex( i ) );

        if (processed_.find(globalFineIndex) != processed_.end() )
          continue;

        if ( exportIndices_ )
        {
          // add only the index of the top level
          indices_.insertEntry(globalFineIndex, globalCoarseIndex);
        }
        else
        {
            // use ISTLBackend wrapper with ()-operator
            matrix_(globalFineIndex,globalCoarseIndex) += coefficients[i];
        }
      }
    }
  }

private:

  const CoarseLocalView& coarseLocalView_;
  const FineLocalView& fineLocalView_;
  const Geometry& geometry_;
  const BitSet& processed_;
  double tolerance_;
  Matrix& matrix_;
  IndexSet& indices_;
  const bool exportIndices_;
};

} // namespace Impl


/** \brief Assemble the transfer matrix for multigrid methods
 *
 *  The coarse and fine basis has to be related: in a way that the fine grid is descendant of the
 *  coarse grid (not necessarily direct descandant).
 *  The two bases are assumed to implement the dune-functions basis interface.
 *
 * \param [out] matrix the block matrix corresponding to the basis transfer operator
 * \param [in] coarseBasis global basis on the coarse grid
 * \param [in] fineBasis global basis on the fine grid
 * \param [in] tolerance (optional) interpolation threshold. Default is 1e-12.
 */
template<class MatrixType, class CoarseBasis, class FineBasis>
static void assembleGlobalBasisTransferMatrix(MatrixType& matrix,
                                              const CoarseBasis& coarseBasis,
                                              const FineBasis& fineBasis,
                                              const double tolerance = 1e-12)
{
  const auto& coarseGridView = coarseBasis.gridView();
  const auto& fineGridView   = fineBasis.gridView();

  auto coarseLocalView = coarseBasis.localView();
  auto fineLocalView   = fineBasis.localView();

  using FineLocalView = std::decay_t<decltype(fineLocalView)>;

  // for multi index access wrap the matrix to the ISTLBackend
  auto matrixBackend = Dune::Fufem::ISTLMatrixBackend(matrix);

  // prepare index set with multi index access
  auto indices = matrixBackend.patternBuilder();
  indices.resize(fineBasis,coarseBasis);

  // ///////////////////////////////////////////
  // Determine the indices present in the matrix
  // ///////////////////////////////////////////

  // Only handle every fine dof once
  std::unordered_set<typename FineLocalView::MultiIndex> processed;

  // loop over all fine grid elements
  for ( const auto& fineElement : elements(fineGridView) )
  {
    // find a coarse element that is the parent of the fine element
    // start in the fine grid and track the geometry mapping
    auto fE = fineElement;
    // collect all geometryInFather's on the way
    std::vector<decltype(fineElement.geometryInFather())> geometryInFathersVector;
    while ( not coarseGridView.contains(fE) and fE.hasFather() )
    {
      // add the geometry to the container
      geometryInFathersVector.push_back(fE.geometryInFather());
      // step up one level
      fE = fE.father();
    }

    // did we really end up in coarseElement?
    if ( not coarseGridView.contains(fE) )
      DUNE_THROW( Dune::Exception, "There is a fine element without a parent in the coarse grid!");

    const auto& coarseElement = fE;

    // geometric mapping fine -> coarse
    const auto geometryInFathers = GeometryInFathers(geometryInFathersVector);

    coarseLocalView.bind(coarseElement);
    fineLocalView.bind(fineElement);

    // visit all children of the coarse node and interpolate with the fine basis functions
    ::Impl::LeafNodeInterpolationVisitor visitor( coarseLocalView, fineLocalView, geometryInFathers,
                                                processed, tolerance, matrixBackend, indices,
                                                true /* = set indexSet */ );

    Dune::TypeTree::applyToTreePair(coarseLocalView.tree(),fineLocalView.tree(),visitor);


    // now the all dof's of this fine element to 'processed'
    for (size_t i=0; i<fineLocalView.size(); i++)
    {
      const auto& globalFineIndex = fineLocalView.index( i );
      processed.insert(globalFineIndex);
    }
  }

  // apply the index set to the matrix
  indices.setupMatrix();
  matrix = 0;

  // reset all dof's
  processed.clear();

  //////////////////////
  // Now set the values
  //////////////////////

  for ( const auto& fineElement : elements(fineGridView) )
  {
    // find a coarse element that is the parent of the fine element
    // start in the fine grid and track the geometry mapping
    auto fE = fineElement;
    // collect all geometryInFather's on the way
    std::vector<decltype(fineElement.geometryInFather())> geometryInFathersVector;
    while ( not coarseGridView.contains(fE) and fE.hasFather() )
    {
      // add the geometry to the container
      geometryInFathersVector.push_back(fE.geometryInFather());
      // step up one level
      fE = fE.father();
    }

    const auto& coarseElement = fE;

    // geometric mapping fine -> coarse
    const auto geometryInFathers = GeometryInFathers(geometryInFathersVector);

    coarseLocalView.bind(coarseElement);
    fineLocalView.bind(fineElement);

    // visit all children of the coarse node and interpolate with the fine basis functions
    ::Impl::LeafNodeInterpolationVisitor visitor( coarseLocalView, fineLocalView, geometryInFathers,
                                                processed, tolerance, matrixBackend, indices,
                                                false /* = increase matrix entries */ );

    Dune::TypeTree::applyToTreePair(coarseLocalView.tree(),fineLocalView.tree(),visitor);

    // now the all dof's of this fine element to 'processed'
    for (size_t i=0; i<fineLocalView.size(); i++)
    {
      const auto& globalFineIndex = fineLocalView.index( i );
      processed.insert(globalFineIndex);
    }
  }
}


#endif
