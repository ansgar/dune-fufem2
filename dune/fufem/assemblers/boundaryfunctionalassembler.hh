// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef BOUNDARY_FUNCTIONAL_ASSEMBLER_HH
#define BOUNDARY_FUNCTIONAL_ASSEMBLER_HH

#include <dune/istl/bvector.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include "dune/fufem/boundarypatch.hh"

/** \brief Generic global functional assembler on a boundary
 *
 * \tparam TrialBasis A finite element basis on the grid, not just on the boundary
 *
 * Here is an example code snippet that shows how to compute the vector with entries
 * \f[
    b_i = \int_\Gamma a \varphi_i \, ds
   \f]
 * where \f$ a \f$ is a constant and \f$ \varphi_i \f$ is the \f$ i \f$-th nodal basis function.
 * \code
   typedef P1NodalBasis<GridView,double> P1Basis;
   P1Basis p1Basis(gridView);
   std::vector<Dune::FieldVector<double,1> > b;   // output vector

   ConstantFunction<Dune::FieldVector<double,dim>,Dune::FieldVector<double,1> > constantOneFunction(a);
   NeumannBoundaryAssembler<typename GridView::Grid, Dune::FieldVector<double,1> > neumannBoundaryAssembler(constantOneFunction);

   BoundaryFunctionalAssembler<P1Basis> boundaryFunctionalAssembler(p1Basis, bottomSurface);
   boundaryFunctionalAssembler.assemble(neumannBoundaryAssembler,
                                        b,
                                        true  // set the output vector to zero before assembly
                                        );
 * \endcode
 */
template <class TrialBasis>
class BoundaryFunctionalAssembler
{
    private:
        typedef typename TrialBasis::GridView GridView;

    public:

        /** \brief Constructor for an assembler for a part of the grid view boundary
         *
         *  Note that you have to make sure that the provided boundary patch and the global basis
         *  belong to the same GridView, since otherwise the call of index() will do something weird.
         * \param TrialBasis A finite element basis on the grid, not just on the boundary
         */
        BoundaryFunctionalAssembler(const TrialBasis& tBasis,const BoundaryPatch<GridView>& boundaryPatch ) :
            tBasis_(tBasis),
            boundaryPatch_(boundaryPatch)
        {}

        /** \brief Assemble
         *
         * \param localAssembler local assembler
         * \param[out] b target vector
         * \param initializeVector If this is set the output vector is
         * set to the correct length and initialized to zero before
         * assembly. Otherwise the assembled values are just added to
         * the vector.
         */
        template <class LocalBoundaryFunctionalAssemblerType, class GlobalVectorType>
        void assemble(LocalBoundaryFunctionalAssemblerType& localAssembler, GlobalVectorType& b, bool initializeVector=true) const
        {
            typedef typename BoundaryPatch<GridView>::iterator BoundaryIterator;

            //typedef typename GridView::template Codim<0>::Iterator ElementIterator;
            typedef typename LocalBoundaryFunctionalAssemblerType::LocalVector LocalVector;
            typedef typename TrialBasis::LinearCombination LinearCombination;

            int rows = tBasis_.size();

            if (initializeVector)
            {
                b.resize(rows);
                std::fill(b.begin(), b.end(), 0.0);
            }

            BoundaryIterator it = boundaryPatch_.begin();
            BoundaryIterator end = boundaryPatch_.end();

            for (; it != end; ++it)
            {
                const auto inside = it->inside();

                // get shape functions
                const typename TrialBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(inside);

                LocalVector localB(tFE.localBasis().size());
                localAssembler.assemble(it, localB, tFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int idx = tBasis_.index(inside, i);
                    const LinearCombination& constraints = tBasis_.constraints(idx);
                    bool isConstrained = tBasis_.isConstrained(idx);
                    if (isConstrained)
                    {
                        for (size_t w=0; w<constraints.size(); ++w)
                            b[constraints[w].index].axpy(constraints[w].factor, localB[i]);
                    }
                    else
                        b[idx] += localB[i];
                }
            }
            return;
        }

    private:
        const TrialBasis& tBasis_;
        const BoundaryPatch<GridView>& boundaryPatch_;
};

#endif

