// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUNCTIONS_COMMON_ISTL_BACKEND_HH
#define DUNE_FUNCTIONS_COMMON_ISTL_BACKEND_HH


#include <dune/common/indices.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>

#include <dune/functions/common/indexaccess.hh>
#include <dune/functions/functionspacebases/hierarchicvectorwrapper.hh>

#include <dune/matrix-vector/traits/scalartraits.hh>


namespace Dune {
namespace Fufem {



template<class Base>
class SingleRowMatrix :
  public Base
{
public:
  using Base::Base;
};

template<class Base>
class SingleColumnMatrix :
  public Base
{
public:
  using Base::Base;
};

template<class Base>
class SingleEntryMatrix :
  public SingleRowMatrix<SingleColumnMatrix<Base>>
{
public:
  using Base::Base;
};



namespace Imp {

constexpr std::false_type isSingleColumnMatrix(const void*)
{ return {}; }

template<class T>
constexpr std::true_type isSingleColumnMatrix(const SingleColumnMatrix<T>*)
{ return {}; }

constexpr std::false_type isSingleRowMatrix(const void*)
{ return {}; }

template<class T>
constexpr std::true_type isSingleRowMatrix(const SingleRowMatrix<T>*)
{ return {}; }

} // namespace Imp


/**
 * \brief Traits class to check if matrix is marked as single column matrix
 */
template<class T>
struct IsSingleColumnMatrix : public decltype(Imp::isSingleColumnMatrix( std::declval<typename std::decay<T>::type*>()))
{};

/**
 * \brief Traits class to check if matrix is marked as single row matrix
 */
template<class T>
struct IsSingleRowMatrix : public decltype(Imp::isSingleRowMatrix( std::declval<typename std::decay<T>::type*>()))
{};



/**
 * \brief Helper class for building matrix pattern
 *
 * This needs to be spezialized for the specific
 * matrix types.
 */
template<class Matrix>
class MatrixBuilder;



/**
 * \brief Helper class for building matrix pattern
 *
 * This spezialization for BCRSMatrix essentially
 * forwards to Dune::MatrixIndexSet.
 */
template<class T, class A>
class MatrixBuilder<Dune::BCRSMatrix<T, A>>
{
public:

  using Matrix = Dune::BCRSMatrix<T, A>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    indices_.resize(rowSizeInfo.size(), colSizeInfo.size());
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_.add(rowIndex[0], colIndex[0]);
  }

  void setupMatrix()
  {
    indices_.exportIdx(matrix_);
  }

private:
  Dune::MatrixIndexSet indices_;
  Matrix& matrix_;
};


/**
 * \brief Helper class for building matrix pattern
 *
 *  Version for MultiTypeBlockMatrix with BCRSMatrix-like blocks.
 *  It uses a 2D std::array of MatrixIndexSets
 */
template<class Row0, class... Rows>
class MatrixBuilder<MultiTypeBlockMatrix<Row0,Rows...>>
{
public:

  using Matrix = MultiTypeBlockMatrix<Row0,Rows...>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    for(size_t i=0; i<rowBlocks_; i++)
      for(size_t j=0; j<colBlocks_; j++)
        indices_[i][j].resize(rowSizeInfo.size({i}), colSizeInfo.size({j}));
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_[rowIndex[0]][colIndex[0]].add(rowIndex[1], colIndex[1]);
  }

  void setupMatrix()
  {
    Hybrid::forEach(Hybrid::integralRange(std::integral_constant<std::size_t,rowBlocks_>()), [&](auto&& i) {
      Hybrid::forEach(Hybrid::integralRange(std::integral_constant<std::size_t,colBlocks_>()), [&](auto&& j) {
        indices_[i][j].exportIdx(matrix_[i][j]);
      });
    });

  }

private:
  //! number of multi type rows
  static constexpr size_t rowBlocks_ = Matrix::N();
  //! number of multi type cols (we assume the matrix is well-formed)
  static constexpr size_t colBlocks_ = Matrix::M();
  //! 2D array of IndexSets
  std::array<std::array<Dune::MatrixIndexSet,colBlocks_>,rowBlocks_> indices_;
  Matrix& matrix_;
};



template<class T, class A>
class MatrixBuilder<Dune::Matrix<Dune::BCRSMatrix<T, A>>>
{
public:

  using Matrix = Dune::Matrix<Dune::BCRSMatrix<T, A>>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    auto rowBlocks = rowSizeInfo.size();
    auto colBlocks = colSizeInfo.size();
    indices_.resize(rowBlocks);
    for(size_t i=0; i<rowBlocks; i++)
    {
      indices_[i].resize(colBlocks);
      for(size_t j=0; j<colBlocks; j++)
        indices_[i][j].resize(rowSizeInfo.size({i}), colSizeInfo.size({j}));
    }
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_[rowIndex[0]][colIndex[0]].add(rowIndex[1], colIndex[1]);
  }

  void setupMatrix()
  {
    auto rowBlocks = indices_.size();
    auto colBlocks = indices_[0].size();
    matrix_.setSize(rowBlocks, colBlocks);
    for(size_t i=0; i<rowBlocks; i++)
      for(size_t j=0; j<colBlocks; j++)
        indices_[i][j].exportIdx(matrix_[i][j]);
  }

private:
  std::vector<std::vector<Dune::MatrixIndexSet>> indices_;
  Matrix& matrix_;
};


template<class M, class E=typename M::field_type>
class ISTLMatrixBackend
{

  // The following is essentially a copy of Dune::Functions::hybridIndexAccess
  // But since matrices do not provide size(), we have to use N() instead.
  template<class C, class I, class F,
    typename std::enable_if< Dune::models<Dune::Functions::Imp::Concept::HasDynamicIndexAccess<I>, C>(), int>::type = 0>
  static auto hybridRowIndexAccess(C&& c, const I& i, F&& f)
    -> decltype(f(c[i]))
  {
    return f(c[i]);
  }

  template<class C, class I, class F,
    typename std::enable_if< not Dune::models<Dune::Functions::Imp::Concept::HasDynamicIndexAccess<I>, C>(), int>::type = 0>
  static decltype(auto) hybridRowIndexAccess(C&& c, const I& i, F&& f)
  {
    return Hybrid::switchCases(std::make_index_sequence<std::decay_t<C>::N()>(), i,
        [&](const auto& ii) -> decltype(auto){
          return f(c[ii]);
        }, [&]() -> decltype(auto){
          return f(c[Dune::Indices::_0]);
        });
  }

  template<class Result, class RowIndex, class ColIndex>
  struct MatrixMultiIndexResolver
  {
    MatrixMultiIndexResolver(const RowIndex& rowIndex, const ColIndex& colIndex) :
      rowIndex_(rowIndex),
      colIndex_(colIndex)
    {}

    template<class C>
    using isReturnable =
    std::integral_constant<bool,
      std::is_convertible<C&, Result>::value or
      Dune::MatrixVector::Traits::ScalarTraits<std::decay_t<C>>::isScalar
    >;

    template<class C>
    auto& toScalar(C&& c) {
      return std::forward<C>(c);
    }

    // Catch the cases where a FieldMatrix<K, 1, 1> was supplied
    auto& toScalar(FieldMatrix<std::decay_t<Result>, 1, 1>& c) {
      return c[0][0];
    }

    auto& toScalar(const FieldMatrix<std::decay_t<Result>, 1, 1>& c) {
      return c[0][0];
    }

    template<class C,
      typename std::enable_if<not isReturnable<C>::value, int>::type = 0,
      typename std::enable_if<IsSingleRowMatrix<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      using namespace Dune::Indices;
      using namespace Dune::Functions;
      auto&& subRowIndex = rowIndex_;
      auto&& subColIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(colIndex_);
      auto&& subIndexResolver = MatrixMultiIndexResolver<Result, decltype(subRowIndex), decltype(subColIndex)>(subRowIndex, subColIndex);
      return static_cast<Result>((hybridRowIndexAccess(c, _0, [&](auto&& ci) -> decltype(auto) {
        return static_cast<Result>((hybridIndexAccess(ci, colIndex_[_0], subIndexResolver)));
      })));
    }

    template<class C,
      typename std::enable_if<not isReturnable<C>::value, int>::type = 0,
      typename std::enable_if<IsSingleColumnMatrix<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      using namespace Dune::Indices;
      using namespace Dune::Functions;
      auto&& subRowIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(rowIndex_);
      auto&& subColIndex = colIndex_;
      auto&& subIndexResolver = MatrixMultiIndexResolver<Result, decltype(subRowIndex), decltype(subColIndex)>(subRowIndex, subColIndex);
      return static_cast<Result>((hybridRowIndexAccess(c, rowIndex_[_0], [&](auto&& ci) -> decltype(auto) {
        return static_cast<Result>((hybridIndexAccess(ci, _0, subIndexResolver)));
      })));
    }

    template<class C,
      typename std::enable_if<not isReturnable<C>::value, int>::type = 0,
      typename std::enable_if<not IsSingleRowMatrix<C>::value, int>::type = 0,
      typename std::enable_if<not IsSingleColumnMatrix<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      using namespace Dune::Indices;
      using namespace Dune::Functions;
      auto&& subRowIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(rowIndex_);
      auto&& subColIndex = Dune::Functions::Imp::shiftedDynamicMultiIndex<1>(colIndex_);
      auto&& subIndexResolver = MatrixMultiIndexResolver<Result, decltype(subRowIndex), decltype(subColIndex)>(subRowIndex, subColIndex);
      return static_cast<Result>((hybridRowIndexAccess(c, rowIndex_[_0], [&](auto&& ci) -> decltype(auto) {
        return static_cast<Result>((hybridIndexAccess(ci, colIndex_[_0], subIndexResolver)));
      })));
    }

    template<class C,
      typename std::enable_if<isReturnable<C>::value, int>::type = 0>
    Result operator()(C&& c)
    {
      return static_cast<Result>(toScalar(std::forward<C>(c)));
    }

    const RowIndex& rowIndex_;
    const ColIndex& colIndex_;
  };



public:

  using Matrix = M;
  using Entry = E;
  using value_type = Entry;

  ISTLMatrixBackend(Matrix& matrix) :
    matrix_(&matrix)
  {}

  MatrixBuilder<Matrix> patternBuilder()
  {
    return {*matrix_};
  }

  template<class RowMultiIndex, class ColMultiIndex>
  const Entry& operator()(const RowMultiIndex& row, const ColMultiIndex& col) const
  {
    MatrixMultiIndexResolver<const Entry&, RowMultiIndex, ColMultiIndex> i(row, col);
    return i(*matrix_);
  }

  template<class RowMultiIndex, class ColMultiIndex>
  Entry& operator()(const RowMultiIndex& row, const ColMultiIndex& col)
  {
    MatrixMultiIndexResolver<Entry&, RowMultiIndex, ColMultiIndex> i(row, col);
    return i(*matrix_);
  }

  /**
   * \brief Const access to wrapped matrix
   */
  const Matrix& matrix() const
  {
    return *matrix_;
  }

  /**
   * \brief Mutable access to wrapped matrix
   */
  Matrix& matrix()
  {
    return *matrix_;
  }

  /**
   * \brief Assign value to wrapped matrix
   */
  template<class Value>
  void assign(const Value& value)
  {
    matrix() = value;
  }

protected:

  Matrix* matrix_;
};



template<class Entry, class Matrix>
auto istlMatrixBackend(Matrix& matrix)
{
  return ISTLMatrixBackend<Matrix, Entry>(matrix);
}

template<class Matrix>
auto istlMatrixBackend(Matrix& matrix)
{
  return ISTLMatrixBackend<Matrix, typename Matrix::field_type>(matrix);
}



template<class Entry, class Vector>
[[deprecated("Use Functions::istlVectorBackend instead of Fufem::istlVectorBackend!")]] auto istlVectorBackend(Vector& vector)
{
  return Dune::Functions::HierarchicVectorWrapper<Vector, Entry>(vector);
}

template<class Vector>
[[deprecated("Use Functions::istlVectorBackend instead of Fufem::istlVectorBackend!")]] auto istlVectorBackend(Vector& vector)
{
  return Dune::Functions::HierarchicVectorWrapper<Vector, typename Vector::field_type>(vector);
}



} // namespace Dune::Fufem
} // namespace Dune



#endif // DUNE_FUNCTIONS_COMMON_ISTL_BACKEND_HH
