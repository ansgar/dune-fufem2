// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef GENERALIZED_LAPLACE_ASSEMBLER_HH
#define GENERALIZED_LAPLACE_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/matrix-vector/axpy.hh>
#include <dune/matrix-vector/traits/utilities.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief Local assembler for Laplace problems with location dependent coefficients, i.e. \f$(A(x)\nabla u,\nabla v) \text{ where } A:\Omega\rightarrow \mathbb R^{d\times d} \text{ with } d\in{1,dim(\Omega)}\f$.
 *
 *  \tparam GridType the type of the underlying grid
 *  \tparam TrialLocalFE the local finite element of the trial space
 *  \tparam AnsatzLocalFE the local finite element of the ansatz space
 *  \tparam FunctionType the type of the coefficient function
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class FunctionType >
class GeneralizedLaplaceAssembler : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<double,1,1> >
{
    private:
        using RangeType = typename FunctionType::RangeType;
        typedef VirtualGridFunction<GridType, RangeType> GridFunctionType;
        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

        // check if coefficient type is either scalar or a matrix with
        // appropriate size
        static void checkCoefficientType() {
            using namespace Dune::MatrixVector;
            if (isScalar<RangeType>())
                return;

            // TODO this check is insufficient for dynamic matrices.
            if (isMatrix<RangeType>()) {
                constexpr int rangeDim = isQuadratic<RangeType>();
                if (not rangeDim)
                  DUNE_THROW(Dune::Exception,
                             "Coefficient function for generalized Laplace was "
                             "detected to be a matrix but is not quadratic or "
                             "cannot be checked statically for this property.");
                if (rangeDim != GridType::dimension)
                  DUNE_THROW(Dune::Exception,
                             "Coefficient function for generalized Laplace has "
                             "wrong dimension of range. Should be scalar or "
                             "quadratic with dimension " << GridType::dimension
                             << ", but has " << rangeDim << ".");
                return;
            }

            DUNE_THROW(Dune::Exception, "Coefficient function for generalized "
                                        "Laplace is neither scalar nor matrix "
                                        "valued.");
            return;
        }

    public:
        //! the block type (template parameter of base class)
        typedef typename Dune::FieldMatrix<double,1,1> T;

        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T >::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T >::BoolMatrix BoolMatrix;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T >::LocalMatrix LocalMatrix;

        /** \brief constructor
         *
         *  \param coeffFunction the coefficient function A(x) (comp. class doc)
         *  \param quadOrder the applied quadrature order
         */
        GeneralizedLaplaceAssembler(const FunctionType& coeffFunction, int quadOrder=3):
            coeffFunction_(coeffFunction),
            quadOrder_(quadOrder)
        {
            checkCoefficientType();

            coeffGridFunction_ = dynamic_cast<const GridFunctionType*>(&coeffFunction_);
        }

        /** \brief After return BoolMatrix isNonZero contains the bit pattern of coupling basis functions
         *
         *  As in a laplace problem all local dofs couple with each other all entries are set to true
         *
         *  \param element the grid element on which to operate
         *  \param isNonZero will contain bit pattern of coupling basis functions after return
         *  \param tFE the local finite element in the trial space used on element
         *  \param aFE the local finite element in the ansatz space used on element
         */
        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
            return;
        }

        /** \brief Assemble the local problem
         *
         *  \param element the grid element on which to operate
         *  \param localMatrix will contain the assembled element matrix
         *  \param tFE the local finite element in the trial space used on element
         *  \param aFE the local finite element in the ansatz space used on element
         */
        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType LocalBasisJacobianType;

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
//            const Dune::template QuadratureRule<double, GridType::dimension>& quad = Dune::template QuadratureRules<double, GridType::dimension>::rule(element.type(), quadOrder_);
            const Dune::template QuadratureRule<double, GridType::dimension>& quad = QuadratureRuleCache<double, GridType::dimension>::rule(element.type(), quadOrder_, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

            // store gradients of shape functions and base functions
            std::vector<LocalBasisJacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<GlobalCoordinate> gradients(tFE.localBasis().size());

            RangeType coeffs(0.0);

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const LocalCoordinate& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const JacobianInverseTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // compute gradients of base functions
                for (size_t i=0; i<gradients.size(); ++i)
                {
                    // transform gradients
                    gradients[i] = 0.0;
                    invJacobian.umv(referenceGradients[i][0], gradients[i]);
                }

                // get coefficient(matrix) at quadrature point
                (coeffGridFunction_!=0) ? coeffGridFunction_->evaluateLocal(element, quadPos, coeffs) : coeffFunction_.evaluate(geometry.global(quadPos),coeffs);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    GlobalCoordinate dummy(0.0);

                    Dune::MatrixVector::addProduct(dummy, coeffs, gradients[i]);

                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = (dummy * gradients[j]) * z;

                        localMatrix[i][j]+= zij;
                        localMatrix[j][i]+= zij;
                    }
                    localMatrix[i][i]+= (dummy * gradients[i]) * z;
                }
            }
            return;
        }

    private:
        const FunctionType& coeffFunction_;
        const GridFunctionType* coeffGridFunction_;

        const int quadOrder_;
};


#endif

