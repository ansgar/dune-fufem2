#ifndef GRADIENT_ASSEMBLER_HH
#define GRADIENT_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/functions/localbasisderivativefunction.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>


//! \todo Please doc me !
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class GradientAssembler
    : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<double,GridType::dimension,1> >
{

        static const size_t dim = GridType::dimension;

    public:
        typedef typename Dune::FieldMatrix<double,dim,1> T;

        typedef typename GridType::template Codim<0>::Entity::Entity Element;
        typedef typename Element::Geometry Geometry;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::BoolMatrix BoolMatrix;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::LocalMatrix LocalMatrix;

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
            return;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix
                  , const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename Dune::template FieldMatrix<double,dim,dim> FMdimdim;
            typedef typename AnsatzLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // compute the element barycenter in local coordinates
            const FVdim pos = Dune::ReferenceElements<double,dim>::general(element.type()).position(0,0);

            // get transposed inverse of Jacobian of transformation
            const FMdimdim& invJacobian = geometry.jacobianInverseTransposed(pos);

            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits FunctionTraits;

            LocalBasisDerivativeFunction<AnsatzLocalFE, FunctionTraits> derivative(aFE.localBasis());

            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());

            std::vector<typename RangeType::field_type> partialDerivatives(tFE.localBasis().size());

            for (size_t i = 0; i < aFE.localBasis().size(); ++i)
            {
                // interpolate all partial derivatives of ansatz function by test functions
                derivative.setIndex(i);
                for (size_t j = 0; j < dim; ++j)
                {
                    derivative.setComponent(j);
                    tFE.localInterpolation().interpolate(derivative, partialDerivatives);

                    for (size_t k = 0; k < tFE.localBasis().size(); ++k)
                        referenceGradients[k][0][j] = partialDerivatives[k];
                }

                // transform gradients
                for (size_t k = 0; k < tFE.localBasis().size(); ++k)
                    invJacobian.mv(referenceGradients[k][0], localMatrix[k][i]);
            }
        }

};


#endif
