// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SECOND_ORDER_OPERATOR_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SECOND_ORDER_OPERATOR_ASSEMBLER_HH

#include <tuple>
#include <type_traits>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/std/apply.hh>

#include <dune/istl/matrix.hh>

#include <dune/functions/common/utility.hh>
#include <dune/functions/gridfunctions/gridfunction.hh>

#include <dune/matrix-vector/axpy.hh>
#include <dune/matrix-vector/transpose.hh>

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include "dune/fufem/assemblers/localoperatorassembler.hh"


#include "dune/fufem/concept.hh"

namespace Dune {
namespace Fufem {
namespace Concept {

    template<class Coord, class... ReturnTypes>
    struct SecondOrderOperatorContraction
    {
        using Args = std::tuple<Coord, Coord, ReturnTypes...>;

        static Args &args;

        template<class F>
        auto require(F&& f) -> decltype(
            Dune::Std::apply(f, args)
        );
    };

    template<class ResultMat, class Scalar, class Coord, class... ReturnTypes>
    struct SecondOrderOperatorAssemblerContraction
        : Refines<SecondOrderOperatorContraction<Coord, ReturnTypes...>>
    {
        using BaseConcept = SecondOrderOperatorContraction<Coord, ReturnTypes...>;

        static ResultMat& r;
        static Scalar& s;
        static typename BaseConcept::Args &args;

        template<class F>
        auto require(F&& f) -> decltype(
            Dune::MatrixVector::addProduct(r, s, Dune::Std::apply(f, args))
        );
    };

}}} // namespace Dune::Fufem::Concept






/**
 * \brief Local assembler for second order differential operator
 *
 * \tparam GridType type of grid to use the assembler for
 * \tparam TrialLocalFE type of trial local finite element space
 * \tparam AnsatzLocalFE type of ansatz local finite element space
 * \tparam Contraction type of contraction function
 * \tparam MB type of matrix blocks of the local matrix
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE,
          class Contraction, class MB=Dune::FieldMatrix<double,1,1>,
          class... FunctionTypes>
class SecondOrderOperatorAssembler :
    public LocalOperatorAssembler <GridType, TrialLocalFE, AnsatzLocalFE, MB >
{
private:
    typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,MB > Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;

    using FunctionTuple = std::tuple<FunctionTypes...>;
    using LocalFunctionTuple = std::tuple<typename FunctionTypes::LocalFunction...>;

#if SECOND_ORDER_OPERATOR_CACHING
    typedef typename Base::Element::Geometry::JacobianTransposed CacheKey;
    class Cache :
        public std::vector<std::tuple<CacheKey, typename Base::LocalMatrix> >
    {
        typedef typename std::vector<std::tuple<CacheKey, typename Base::LocalMatrix> > BaseContainer;
    public:

        bool restore(const CacheKey& key, typename Base::LocalMatrix& matrix) const
        {
            auto it = std::find_if(this->begin(), this->end(), [&](const typename BaseContainer::value_type& other) {
                    return (CacheKey(key) -= std::get<0>(other)).infinity_norm() <= 1e-15;
                });
            if (it != this->end())
            {
                matrix = std::get<1>(*it);
                return true;
            }
            return false;
        }

        void store(const CacheKey& key, typename Base::LocalMatrix& matrix)
        {
            if (this->size() < 10)
                this->push_back(std::make_tuple(key, matrix));
        }
    };
#endif

public:

    typedef typename Base::Element Element;
    typedef typename Element::Geometry Geometry;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;
    typedef typename Base::MatrixEntry MatrixEntry;

    typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename Dune::template FieldVector<double,dimworld> WorldCoordinate;

    /**
     * \brief Create SecondOrderOperatorAssembler from contraction
     *
     * \param contraction Functor doing the gradient contraction
     * \param isSymmetric Flag to pass a-priory knowledge on symmetry
     *
     * The assembler will apply compute the integrals over contraction(Dgi, Dgj)
     * where Dgi and Dgj are the gradients of the i-th and j-th scalar
     * local basis function and write the results to the (j,i)-th entry
     * of the local matrix. Notice that the result of the contraction
     * can be any matrix type for multi component ansatz and test spaces
     * with tensor structure. You only need to ensure that you can do
     * addProduct(ME, s, contraction(Dgi, Dgj)) where ME is a MatrixEntry
     * and s is a scalar.
     *
     * If isSymmetric is set, the assembler will
     * any compute one triangle of the local matrix and copy it
     * to the other one.
     */
    SecondOrderOperatorAssembler(const Contraction& contraction,
                                 bool isSymmetric,
                                 QuadratureRuleKey coefficientQuadKey,
                                 FunctionTypes... functions) :
        contraction_(contraction),
        isSymmetric_(isSymmetric),
        coefficientQuadKey_(coefficientQuadKey),
        localFunctions_(Dune::Functions::transformTuple(
                        [](auto&& f) { return localFunction(f); },
                        std::forward_as_tuple(functions...)))
    {
        using ContractionConcept
            = Dune::Fufem::Concept::SecondOrderOperatorAssemblerContraction<
                MatrixEntry, double, WorldCoordinate,
                typename std::result_of<FunctionTypes(WorldCoordinate)>::type...>;
        static_assert(
            Dune::Fufem::Concept::models<ContractionConcept, Contraction>(),
            "Contraction type does not support: Dune::MatrixVector::addProduct(MatrixEntry, double, contraction(WorldCoordinate, WorldCoordinate, ...))");
    }
    SecondOrderOperatorAssembler(const Contraction& contraction,
                                 bool isSymmetric,
                                 FunctionTypes... functions) :
        SecondOrderOperatorAssembler(contraction, isSymmetric,
                                     QuadratureRuleKey(dim, 0), functions...)
    {}
    SecondOrderOperatorAssembler(const Contraction& contraction,
                                 FunctionTypes... functions) :
        SecondOrderOperatorAssembler(contraction, false, functions...)
    {}

    void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        isNonZero = true;
    }

    /** \brief Assemble the local stiffness matrix for a given element
     */
    void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        // Make sure we got suitable shape functions
        assert(tFE.type() == element.type());
        assert(aFE.type() == element.type());

        // check if ansatz local fe = test local fe
        if (not Base::isSameFE(tFE, aFE))
            DUNE_THROW(Dune::NotImplemented, "SecondOrderOperatorAssembler is only implemented for ansatz space=test space!");

        int rows = localMatrix.N();
        int cols = localMatrix.M();

        localMatrix = 0.0;

        // get geometry and store it
        const Geometry geometry = element.geometry();

#if SECOND_ORDER_OPERATOR_CACHING
        auto cacheKey = geometry.jacobianTransposed(typename Geometry::LocalCoordinate(0.0));
        if (cache_.restore(cacheKey, localMatrix))
            return;
#endif

        // get quadrature rule
        QuadratureRuleKey quadKey
            = QuadratureRuleKey(tFE).derivative().square().product(coefficientQuadKey_);
        const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

        // store gradients of shape functions and base functions
        referenceGradients_.resize(tFE.localBasis().size());
        gradients_.resize(tFE.localBasis().size());

        using namespace Dune::Hybrid;
        forEach(integralRange(size(localFunctions_)),
                [&](auto&& i) { elementAt(localFunctions_, i).bind(element); });

        // loop over quadrature points
        for (auto&& quadPoint : quad)
        {
            // get quadrature point
            const auto& quadPosition = quadPoint.position();

            // get transposed inverse of Jacobian of transformation
            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPosition);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPosition, referenceGradients_);

            // transform gradients
            for (size_t i=0; i<gradients_.size(); ++i)
                invJacobian.mv(referenceGradients_[i][0], gradients_[i]);

            // get integration factor
            auto z = quadPoint.weight() * geometry.integrationElement(quadPosition);

            std::tuple<typename std::result_of<FunctionTypes(
                WorldCoordinate)>::type...>
                returnValues;
            forEach(integralRange(size(localFunctions_)), [&](auto&& i) {
              elementAt(returnValues, i) =
                  elementAt(localFunctions_, i)(quadPosition);
            });
            for (int i=0; i<rows; ++i) {
                int const startingIndex = isSymmetric_ ? i : 0;
                for (int j = startingIndex; j < cols; ++j) {
                    const auto boundContraction = [&](auto... args) {
                        return contraction_(gradients_[i], gradients_[j], args...);
                    };
                    Dune::MatrixVector::addProduct(
                        localMatrix[i][j], z,
                        Dune::Std::apply(boundContraction, returnValues));
                }
            }
        }
        if (isSymmetric_)
        {
            for (int i=0; i<rows; ++i)
                for (int j=0; j<i; ++j)
                    Dune::MatrixVector::transpose(localMatrix[j][i], localMatrix[i][j]);
        }
#if SECOND_ORDER_OPERATOR_CACHING
        cache_.store(cacheKey, localMatrix);
#endif
    }

protected:
    const Contraction contraction_;
    bool isSymmetric_;
    const QuadratureRuleKey coefficientQuadKey_;
    mutable LocalFunctionTuple localFunctions_;

    mutable std::vector<JacobianType> referenceGradients_;
    mutable std::vector<WorldCoordinate> gradients_;
#if SECOND_ORDER_OPERATOR_CACHING
    mutable Cache cache_;
#endif
};


#endif
