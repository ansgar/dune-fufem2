// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUBGRID_GENERALIZED_LAPLACE_ASSEMBLER_HH
#define SUBGRID_GENERALIZED_LAPLACE_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/matrix-vector/arithmetic.hh>
#include <dune/matrix-vector/matrixtraits.hh>
#include <dune/matrix-vector/scalartraits.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>

/** \brief Local assembler for Laplace problems with location dependent coefficients, i.e. \f$(A(x)\nabla u,\nabla v) \text{ where } A:\Omega\rightarrow \mathbb R^{d\times d} \text{ with } d\in{1,dim(\Omega)}\f$.
 *
 *  \tparam GridType the type of the underlying grid
 *  \tparam TrialLocalFE the local finite element of the trial space
 *  \tparam AnsatzLocalFE the local finite element of the ansatz space
 *  \tparam FunctionType the type of the coefficient function
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class FunctionType >
class SubgridGeneralizedLaplaceAssembler : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<double,1,1> >
{
    public:
        //! the block type (template parameter of base class)
        typedef typename Dune::FieldMatrix<double,1,1> T;
    private:
        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;

        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

        typedef typename GridType::HostGridType HostGridType;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;
//        typedef typename DerivativeTypefier<GlobalCoordinate,T>::DerivativeType FRangeType;
//        typedef VirtualGridFunction<GridType, FRangeType> GridFunction;
//        typedef VirtualGridFunction<HostGridType, FRangeType> HostGridFunction;

        typedef BasisGridFunctionInfo<HostGridType> HostBasisGridFunctionInfo;

        typedef VirtualGridFunction<HostGridType, typename FunctionType::RangeType> HostGridFunction;

        typedef typename GridType::HostGridType::template Codim<0>::Entity::Geometry HostGeometry;

        // check if coefficient type is either scalar or a matrix with appropriate size
        static void checkCoefficientType()
        {
            typedef typename FunctionType::RangeType RangeType;
            if (not(Dune::MatrixVector::ScalarTraits<RangeType>::isScalar))
            {
                // check if non-scalar coefficient type is a matrix type
                if (not(Dune::MatrixVector::MatrixTraits<RangeType>::isMatrix))
                    DUNE_THROW(Dune::Exception,"Coefficient function for generalized Laplace is neither scalar nor matrix valued.");

                // check if matrix coefficient type has proper dimensions
                int crows = Dune::MatrixVector::MatrixTraits<RangeType>::rows;
                int ccols = Dune::MatrixVector::MatrixTraits<RangeType>::cols;
                if (not(crows==GridType::dimension and ccols==GridType::dimension))
                    DUNE_THROW(Dune::Exception,"Coefficient function for generalized Laplace has wrong dimension of range.\n Should be 1x1 or " << GridType::dimension << "x" << GridType::dimension << ", is " << crows << "x" << ccols << ".");
            }
        }

    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        /** \brief constructor
         *
         *  \param coeffFunction the coefficient function A(x) (comp. class doc)
         *  \param quadOrder the applied quadrature order
         */
        SubgridGeneralizedLaplaceAssembler(const FunctionType& coeffFunction, const GridType& grid, int quadOrder=3):
            coeffFunction_(coeffFunction),
            hostBasisGridFunctionInfo_(dynamic_cast<const HostBasisGridFunctionInfo*>(&coeffFunction_)),
            coeffHostGridFunction_(dynamic_cast<const HostGridFunction*>(&coeffFunction_)),
            grid_(grid),
            quadOrder_(quadOrder)
        {
            checkCoefficientType();
        }

        /** \brief After return BoolMatrix isNonZero contains the bit pattern of coupling basis functions
         *
         *  As in a laplace problem all local dofs couple with each other all entries are set to true
         *
         *  \param element the grid element on which to operate
         *  \param isNonZero will contain bit pattern of coupling basis functions after return
         *  \param tFE the local finite element in the trial space used on element
         *  \param aFE the local finite element in the ansatz space used on element
         */
        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
            return;
        }

        /** \brief Assemble the local problem
         *
         *  \param element the grid element on which to operate
         *  \param localMatrix will contain the assembled element matrix
         *  \param tFE the local finite element in the trial space used on element
         *  \param aFE the local finite element in the ansatz space used on element
         */
        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename GridType::template Codim<0>::Geometry::Jacobian GeoJacobianInvTransposed;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType LocalBasisJacobianType;

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            const auto hostelement = grid_.template getHostEntity<0>(element);

            bool trialSpaceIsRefined = IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE);

            // store gradients of shape functions and base functions
            std::vector<LocalBasisJacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<GlobalCoordinate> gradients(tFE.localBasis().size());

            typename FunctionType::RangeType coeffs(0.0);

            if (hostelement->isLeaf())
            {
                bool hostGFIsRefined = hostBasisGridFunctionInfo_ and hostBasisGridFunctionInfo_->isRefinedLocalFiniteElement(*hostelement);

                // get quadrature rule
                const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(element.type(), quadOrder_, (trialSpaceIsRefined or hostGFIsRefined) );

                // loop over quadrature points
                for (size_t pt=0; pt < quad.size(); ++pt)
                {
                    // get quadrature point
                    const LocalCoordinate& quadPos = quad[pt].position();

                    // get transposed inverse of Jacobian of transformation
                    const GeoJacobianInvTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                    // get integration factor
                    const double integrationElement = geometry.integrationElement(quadPos);

                    // get gradients of shape functions
                    tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                    // transform gradients
                    for (size_t i=0; i<gradients.size(); ++i)
                        invJacobian.mv(referenceGradients[i][0], gradients[i]);

                    // get coefficient(matrix) at quadrature point
                    (coeffHostGridFunction_!=0) ? coeffHostGridFunction_->evaluateLocal(*hostelement, quadPos, coeffs) : coeffFunction_.evaluate(geometry.global(quadPos),coeffs);

                    // compute matrix entries
                    double z = quad[pt].weight() * integrationElement;
                    for (int i=0; i<rows; ++i)
                    {
                        GlobalCoordinate dummy(0.0);

                        Dune::MatrixVector::addProduct(dummy, coeffs, gradients[i]);

                        for (int j=i+1; j<cols; ++j)
                        {
                            double zij = (dummy * gradients[j]) * z;

                            localMatrix[i][j]+= zij;
                            localMatrix[j][i]+= zij;
                        }
                        localMatrix[i][i]+= (dummy * gradients[i]) * z;
                    }
                }
            }
            else // corresponding hostgrid element is not in hostgrid leaf
            {
                typename GridType::HostGridType& hostgrid = grid_.getHostGrid();
                typename GridType::HostGridType::HierarchicIterator lastDescendant = hostelement->hend(hostgrid.maxLevel());
                typename GridType::HostGridType::HierarchicIterator descElement = hostelement->hbegin(hostgrid.maxLevel());

                for (; descElement!=lastDescendant; ++descElement)
                {
                    if (descElement->isLeaf())
                    {
                        const HostGeometry hostGeometry = descElement->geometry();

                        bool hostGFIsRefined = hostBasisGridFunctionInfo_ and hostBasisGridFunctionInfo_->isRefinedLocalFiniteElement(*descElement);

                        // get quadrature rule
                        const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(descElement->type(), quadOrder_, hostGFIsRefined);

                        // loop over quadrature points
                        for (size_t pt=0; pt < quad.size(); ++pt)
                        {
                            // get quadrature point
                            const LocalCoordinate& quadPos = quad[pt].position();
                            const LocalCoordinate quadPosInSubgridElement = geometry.local(hostGeometry.global(quadPos)) ;

                            // get transposed inverse of Jacobian of transformation
                            const GeoJacobianInvTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                            // get integration factor
                            const double integrationElement = hostGeometry.integrationElement(quadPos);

                            // get gradients of shape functions
                            tFE.localBasis().evaluateJacobian(quadPosInSubgridElement, referenceGradients);

                            // transform gradients
                            for (size_t i=0; i<gradients.size(); ++i)
                                invJacobian.mv(referenceGradients[i][0], gradients[i]);

                            // get coefficient(matrix) at quadrature point
                            (coeffHostGridFunction_!=0) ? coeffHostGridFunction_->evaluateLocal(*descElement, quadPos, coeffs) : coeffFunction_.evaluate(geometry.global(quadPos),coeffs);

                            // compute matrix entries
                            double z = quad[pt].weight() * integrationElement;
                            for (int i=0; i<rows; ++i)
                            {
                                GlobalCoordinate dummy(0.0);

                                Dune::MatrixVector::addProduct(dummy, coeffs, gradients[i]);

                                for (int j=i+1; j<cols; ++j)
                                {
                                    double zij = (dummy * gradients[j]) * z;

                                    localMatrix[i][j]+= zij;
                                    localMatrix[j][i]+= zij;
                                }
                                localMatrix[i][i]+= (dummy * gradients[i]) * z;
                            }
                        }
                    }
                }
            }

            return;
        }

    private:
        const FunctionType& coeffFunction_;
        const HostGridFunction* coeffHostGridFunction_;
        const HostBasisGridFunctionInfo* hostBasisGridFunctionInfo_;

        const GridType& grid_;

        const int quadOrder_;
};


#endif

