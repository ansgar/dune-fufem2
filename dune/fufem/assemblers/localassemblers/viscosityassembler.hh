#ifndef VISCOSITY_ASSEMBLER_HH
#define VISCOSITY_ASSEMBLER_HH

#include <memory>

#include "dune/fufem/assemblers/localassemblers/secondorderoperatorassembler.hh"
#include "dune/fufem/assemblers/localassemblers/strainproductassembler.hh"
#include "dune/fufem/functions/analyticgridfunction.hh"
#include "dune/fufem/symmetrictensor.hh"

#include <dune/functions/gridfunctions/gridviewfunction.hh>

template <class GradientVector, class LocalMatrix>
inline LocalMatrix isotropicNewtonianViscosityContraction(
    const GradientVector& g1, const GradientVector& g2,
    typename GradientVector::field_type muShear,
    typename GradientVector::field_type muBulk) {
    using ctype = typename GradientVector::field_type;
    int const dim = GradientVector::dimension;
    LocalMatrix R = Dune::ScaledIdentityMatrix<ctype,dim>(muShear*(g1*g2));
    for(std::size_t i=0; i<R.N(); ++i)
      for(std::size_t j=0; j<R.N(); ++j)
        R[i][j] += muShear * (g1[j]*g2[i])
          + (muBulk-(2.0/3.0)*muShear) * (g1[i]*g2[j]);
    return R;
}

template <class Grid, class LFE, class LocalMatrix>
auto getIsotropicNewtonianViscosityAssembler(Grid const& grid, double muShear,
                                             double muBulk) {
  using GlobalCoordinate =
      typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
  // decltype(contraction) needs to be a lambda rather than a function pointer
  // in order to allow for it to be inlined
  auto const contraction = [](auto&&... args) {
    return isotropicNewtonianViscosityContraction<GlobalCoordinate,
                                                  LocalMatrix>(
        std::forward<decltype(args)>(args)...);
  };
  size_t const dim = GlobalCoordinate::dimension;
  auto const leafView = grid.leafGridView();

  auto funcMuShear = [muShear](GlobalCoordinate const &) {return muShear;};
  auto funcMuBulk = [muBulk](GlobalCoordinate const &) {return muBulk;};
  auto gridFuncMuShear = Dune::Functions::makeGridViewFunction(funcMuShear, leafView);
  auto gridFuncMuBulk = Dune::Functions::makeGridViewFunction(funcMuBulk, leafView);

  return SecondOrderOperatorAssembler<Grid, LFE, LFE, decltype(contraction),
                                      LocalMatrix, decltype(gridFuncMuShear),
                                      decltype(gridFuncMuBulk)>(
      contraction, true, QuadratureRuleKey(dim, 0), gridFuncMuShear,
      gridFuncMuBulk);
}

template <class Grid, class LFE, class LocalMatrix, class F1, class F2>
auto getIsotropicNewtonianViscosityAssembler(
    Grid const& grid, const F1& muShear, const F2& muBulk,
    QuadratureRuleKey quadratureRuleKey) {
  using GlobalCoordinate = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
  // decltype(contraction) needs to be a lambda rather than a function pointer
  // in order to allow for it to be inlined
  auto const contraction = [](auto&&... args) {
    return isotropicNewtonianViscosityContraction<GlobalCoordinate,
                                                  LocalMatrix>(
        std::forward<decltype(args)>(args)...);
  };
  auto const leafView = grid.leafGridView();

  auto gridFuncMuShear = Dune::Functions::makeGridViewFunction(muShear, leafView);
  auto gridFuncMuBulk = Dune::Functions::makeGridViewFunction(muBulk, leafView);

  return SecondOrderOperatorAssembler<Grid, LFE, LFE, decltype(contraction),
                                      LocalMatrix, decltype(gridFuncMuShear),
                                      decltype(gridFuncMuBulk)>(
      contraction, true, quadratureRuleKey, gridFuncMuShear, gridFuncMuBulk);
}

/** \brief Local assembler for the viscous part of the linear visco-elastic Kelvin-Voigt material. */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class ViscosityAssembler
    : public StrainProductAssembler< GridType, TrialLocalFE, AnsatzLocalFE, ViscosityAssembler<GridType,TrialLocalFE,AnsatzLocalFE> >
{
    private:
        typedef StrainProductAssembler< GridType, TrialLocalFE, AnsatzLocalFE, ViscosityAssembler<GridType,TrialLocalFE,AnsatzLocalFE> > SPA;
        using SPA::dim;
        using typename SPA::ctype;

        /** \brief shear viscosity */
        const ctype muShear_;

        /**  \brief bulk viscosity */
        const ctype muBulk_;

    public:
        using typename SPA::T;
        using typename SPA::BoolMatrix;
        using typename SPA::Element;
        using typename SPA::LocalMatrix;

    ViscosityAssembler(ctype muShear, ctype muBulk):
        muShear_(muShear), muBulk_(muBulk)
    {}

    SymmetricTensor<dim> strainToStress(const SymmetricTensor<dim>& strain,
                                        const Element&,
                                        const Dune::FieldVector<ctype, dim>&) const {
            SymmetricTensor<dim> stress = strain;
            stress *= 2*muShear_;
            stress.addToDiag((muBulk_-(2.0/3.0)*muShear_) * strain.trace());
            return stress;
    }


};

/** \brief Local assembler for the viscous part of the linear visco-elastic Kelvin-Voigt material. With variable coefficients. */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class FunctionType>
class VariableCoefficientViscosityAssembler
    : public StrainProductAssembler< GridType, TrialLocalFE, AnsatzLocalFE, VariableCoefficientViscosityAssembler<GridType,TrialLocalFE,AnsatzLocalFE,FunctionType> >
{
    private:
        typedef StrainProductAssembler< GridType, TrialLocalFE, AnsatzLocalFE, VariableCoefficientViscosityAssembler<GridType,TrialLocalFE,AnsatzLocalFE, FunctionType> > SPA;
        using SPA::dim;
        using typename SPA::ctype;
        typedef typename FunctionType::RangeType RangeType;
        typedef VirtualGridFunction<GridType, RangeType> GridFunction;

    public:
        using typename SPA::T;
        using typename SPA::BoolMatrix;
        using typename SPA::Element;
        using typename SPA::LocalMatrix;

    VariableCoefficientViscosityAssembler(const GridType& grid,
                                          const FunctionType& muShear,
                                          const FunctionType& muBulk,
                                          const QuadratureRuleKey& muShearKey,
                                          const QuadratureRuleKey& muBulkKey)
      : SPA(muShearKey.sum(muBulkKey)),
        muShearGridFunctionP_(makeGridFunction(grid, muShear)),
        muBulkGridFunctionP_(makeGridFunction(grid, muBulk))
    {}

    SymmetricTensor<dim> strainToStress(const SymmetricTensor<dim>& strain,
                                        const Element& element,
                                        const Dune::FieldVector<ctype, dim>& position) const {
            RangeType muBulk;
            muBulkGridFunctionP_->evaluateLocal(element, position, muBulk);
            RangeType muShear;
            muShearGridFunctionP_->evaluateLocal(element, position, muShear);

            SymmetricTensor<dim> stress = strain;
            stress *= 2*muShear;
            stress.addToDiag((muBulk-(2.0/3.0)*muShear) * strain.trace());
            return stress;
    }
    protected:
        std::shared_ptr<const GridFunction> muShearGridFunctionP_;
        std::shared_ptr<const GridFunction> muBulkGridFunctionP_;
};
#endif
