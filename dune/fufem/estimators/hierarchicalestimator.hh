#ifndef HIERARCHICAL_ESTIMATOR_HH
#define HIERARCHICAL_ESTIMATOR_HH

#include <dune/common/function.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bdmatrix.hh>

#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include "hierarchicestimatorbase.hh"
#include "refinementindicator.hh"

template <class EnlargedGlobalBasis, int blocksize>
class HierarchicalEstimator
{
    typedef typename EnlargedGlobalBasis::GridView::Grid GridType;
    typedef typename EnlargedGlobalBasis::LocalFiniteElement Lfe;

    enum {dim = GridType::dimension};
    enum {dimworld = GridType::dimensionworld};

    typedef typename EnlargedGlobalBasis::ReturnType field_type;

    typedef Dune::BlockVector<Dune::FieldVector<field_type, blocksize> > VectorType;
    typedef Dune::FieldMatrix<field_type, blocksize,blocksize> MatrixBlockType;
    typedef Dune::BDMatrix<MatrixBlockType> DiagonalMatrixType;

    typedef Dune::VirtualFunction<Dune::FieldVector<field_type,dimworld>, Dune::FieldVector<field_type,blocksize> > FunctionType;
public:

    HierarchicalEstimator(GridType& grid)
        : grid_(&grid)
    {}

    void estimate(const VectorType& x,
                  const FunctionType* volumeTerm,
                  const FunctionType* neumannTerm,
                  const BoundaryPatch<typename GridType::LeafGridView>& dirichletBoundary,
                  RefinementIndicator<GridType>& refinementIndicator,
                  LocalOperatorAssembler<GridType, Lfe, Lfe,MatrixBlockType>* localStiffness)
    {
        // embed x into a second order fe space with the nodal basis
        assert(x.size() == grid_->leafIndexSet().size(dim));

        VectorType xP2;

        P1NodalBasis<typename GridType::LeafGridView, field_type> p1NodalBasis(grid_->leafGridView());
        EnlargedGlobalBasis enlargedGlobalBasis(grid_->leafGridView());

        Functions::interpolate(enlargedGlobalBasis, xP2, Functions::makeFunction(p1NodalBasis, x));

        // /////////////////////////////////////////////////
        //   Assemble the preconditioned defect problem
        // /////////////////////////////////////////////////

        VectorType residual;
        DiagonalMatrixType matrix;

        HierarchicEstimatorBase<EnlargedGlobalBasis,blocksize,field_type>::preconditionedDefectProblem(enlargedGlobalBasis, xP2, volumeTerm, neumannTerm, residual, matrix, localStiffness);

        // ///////////////////////////////////////////////////////////
        //   Construct the set of Dirichlet dofs for the enlarged problem
        // ///////////////////////////////////////////////////////////

        Dune::BitSetVector<blocksize> enlargedSpaceDirichletNodes;

        constructBoundaryDofs(dirichletBoundary, enlargedGlobalBasis, enlargedSpaceDirichletNodes);

        // /////////////////////////////////////////////////
        //   Solve the preconditioned defect problem
        // /////////////////////////////////////////////////

        hierarchicalError_.resize(enlargedGlobalBasis.size());

#warning Stick/slip-conditions not implemented properly
        for (size_t i=0; i<enlargedGlobalBasis.size(); i++)
            if (enlargedSpaceDirichletNodes[i][0])
                hierarchicalError_[i] = 0;
            else
                matrix[i][i].solve(hierarchicalError_[i], residual[i]);

        // //////////////////////////////////////////////////////////////////////
        //   Transfer the hierarchical extension to the RefinementIndicator
        // //////////////////////////////////////////////////////////////////////

        refinementIndicator.clear();

        // Extract all contributions that correspond to edges
        for (const auto& e : elements(grid_->leafGridView())) {

            const Lfe& lfe = enlargedGlobalBasis.getLocalFiniteElement(e);

            int numLocalDofs = lfe.localCoefficients().size();

            for (int j=0; j<numLocalDofs; j++) {

                int codim     = lfe.localCoefficients().localKey(j).codim();
                int subEntity = lfe.localCoefficients().localKey(j).subEntity();

                unsigned int globalIdx = enlargedGlobalBasis.index(e, j);

                if (codim==dim)
                    assert(hierarchicalError_[globalIdx].two_norm() < 1e-5);

                refinementIndicator.set(e, codim, subEntity, hierarchicalError_[globalIdx].two_norm());

            }

        }

    }

    template <class NonlinearProblemType>
    void estimate(const VectorType& x,
            const BoundaryPatch<typename GridType::LeafGridView>& dirichletBoundary,
            RefinementIndicator<GridType>& refinementIndicator,
            NonlinearProblemType& problem)
    {
        // embed x into a second order fe space with the nodal basis
        assert(x.size() == grid_->leafIndexSet().size(dim));

        VectorType xP2;

        P1NodalBasis<typename GridType::LeafGridView, field_type> p1NodalBasis(grid_->leafGridView());
        EnlargedGlobalBasis enlargedGlobalBasis(grid_->leafGridView());

        Functions::interpolate(enlargedGlobalBasis, xP2, Functions::makeFunction(p1NodalBasis, x));

        // /////////////////////////////////////////////////
        //   Assemble the preconditioned defect problem
        // /////////////////////////////////////////////////

        VectorType residual;
        DiagonalMatrixType matrix;

        problem.template assembleDefectQP<DiagonalMatrixType>(xP2, residual, matrix);

        // ///////////////////////////////////////////////////////////
        //   Construct the set of Dirichlet dofs for the enlarged problem
        // ///////////////////////////////////////////////////////////

        Dune::BitSetVector<blocksize> enlargedSpaceDirichletNodes;

        constructBoundaryDofs(dirichletBoundary, enlargedGlobalBasis, enlargedSpaceDirichletNodes);

        // /////////////////////////////////////////////////
        //   Solve the preconditioned defect problem
        // /////////////////////////////////////////////////

        hierarchicalError_.resize(enlargedGlobalBasis.size());

#warning Stick/slip-conditions not implemented properly
        for (size_t i=0; i<enlargedGlobalBasis.size(); i++)
            if (enlargedSpaceDirichletNodes[i][0])
                hierarchicalError_[i] = 0;
            else
                matrix[i][i].solve(hierarchicalError_[i], residual[i]);

        // //////////////////////////////////////////////////////////////////////
        //   Transfer the hierarchical extension to the RefinementIndicator
        // //////////////////////////////////////////////////////////////////////

        refinementIndicator.clear();

        // Extract all contributions that correspond to edges
        for (const auto& e : elements(grid_->leafGridView())) {

            const Lfe& lfe = enlargedGlobalBasis.getLocalFiniteElement(e);
            int numLocalDofs = lfe.localCoefficients().size();

            for (int j=0; j<numLocalDofs; j++) {

                int codim     = lfe.localCoefficients().localKey(j).codim();
                int subEntity = lfe.localCoefficients().localKey(j).subEntity();

                unsigned int globalIdx = enlargedGlobalBasis.index(e, j);

             //   if (codim==dim)
              //      assert(hierarchicalError_[globalIdx].two_norm() < 1e-5);

                refinementIndicator.set(e, codim, subEntity, hierarchicalError_[globalIdx].two_norm());

            }

        }
    }

    double getErrorSquared(LocalOperatorAssembler<GridType, Lfe, Lfe,MatrixBlockType>* localStiffness) const
    {
        EnlargedGlobalBasis enlargedGlobalBasis(grid_->leafGridView());
        return HierarchicEstimatorBase<EnlargedGlobalBasis,blocksize,field_type>::energyNormSquared(enlargedGlobalBasis,
                hierarchicalError_, *localStiffness);
    }

protected:

    // /////////////////////////////////
    //   Data members
    // /////////////////////////////////

    GridType* grid_;

    VectorType hierarchicalError_;

};

#endif
