// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_FUFEM_FUNCTIONS_COARSE_GRID_FUNCTION_WRAPPER_HH
#define DUNE_FUFEM_FUNCTIONS_COARSE_GRID_FUNCTION_WRAPPER_HH


#include <dune/fufem/functions/virtualgridfunction.hh>



/**
 * \brief A wrapper to allow evaluation of grid function on finer grid
 *
 * If the wrapped grid function can be evaluated on an element the wrapper
 * grid function can be evaluated on this element and all descendants.
 *
 * \tparam GF Type of wrapped coarse grid function
 */
template<class GF>
class CoarseGridFunctionWrapper :
    public VirtualGridFunction<typename GF::Grid, typename GF::RangeType>
{
        typedef VirtualGridFunction<typename GF::Grid, typename GF::RangeType> BaseType;

    public:
        typedef typename BaseType::LocalDomainType LocalDomainType;
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;
        typedef typename BaseType::Element Element;
        typedef typename BaseType::Grid Grid;

        typedef GF CoarseGridFunction;


        /**
         * \brief Setup grid function
         *
         * \param f The coarse grid function
         */
        CoarseGridFunctionWrapper(const CoarseGridFunction& f) :
            BaseType(f.grid()),
            f_(&f)
        {}



        /**
         * \copydoc VirtualGridFunction::evaluateLocal
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            if (f_->isDefinedOn(e))
            {
                f_->evaluateLocal(e, x, y);
            }
            else
            {
                Element ancestor(e);
                LocalDomainType xCoarse;

                auto r = findAncestorInCoarseGridView(ancestor, x, xCoarse);
                assert(r);

                f_->evaluateLocal(ancestor, xCoarse, y);
            }
        }



        /**
         * \copydoc VirtualGridFunction::evaluateDerivativeLocal
         */
        virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
        {
            if (f_->isDefinedOn(e))
            {
                f_->evaluateDerivativeLocal(e, x, d);
            }
            else
            {
                Element ancestor(e);
                LocalDomainType xCoarse;

                auto r = findAncestorInCoarseGridView(ancestor, x, xCoarse);
                assert(r);

                f_->evaluateDerivativeLocal(ancestor, xCoarse, d);
            }
        }


        /**
         * \copydoc VirtualGridFunction::isDefinedOn
         */
        virtual bool isDefinedOn(const Element& e) const
        {
            if (e.isLeaf() or f_->isDefinedOn(e))
                return true;
            Element ep(e);
            return findAncestorInCoarseGridView(ep);
        }

    protected:

        bool findAncestorInCoarseGridView(Element& e) const
        {
            while (not(f_->isDefinedOn(e)))
            {
                if (not(e.hasFather()))
                    return false;
                e = e.father();
            }
            return true;
        }

        // Find the anchestor of e in the coarse grid view and transform
        // local coordinates to local coordinates w.r.t. the ancestor
        bool findAncestorInCoarseGridView(Element& e, LocalDomainType xFine, LocalDomainType& xCoarse) const
        {
            while (not(f_->isDefinedOn(e)))
            {
                if (not(e.hasFather()))
                    return false;
                xCoarse = e.geometryInFather().global(xFine);
                xFine = xCoarse;
                e = e.father();
            }
            return true;
        }

        const CoarseGridFunction* f_;
};


#endif

