install(FILES
    basisidmapper.hh
    basisinterpolator.hh
    boundarydofs.hh
    functionintegrator.hh
    gradientbasis.hh
    gridfunctionadaptor.hh
    namedfunctionmap.hh
    p0p1interpolation.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fufem/functiontools)
