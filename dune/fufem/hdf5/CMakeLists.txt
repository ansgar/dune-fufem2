install(FILES
  attributes.hh
  file.hh
  frombuffer.hh
  sequenceio.hh
  singletonwriter.hh
  tobuffer.hh
  typetraits.hh
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fufem/hdf5)
