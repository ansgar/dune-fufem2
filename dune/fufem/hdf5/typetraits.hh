#ifndef DUNE_FUFEM_HDF5_TYPETRAITS_HH
#define DUNE_FUFEM_HDF5_TYPETRAITS_HH

namespace HDF5 {
template <typename ctype>
struct TypeTraits {
  static hid_t getType() {
    DUNE_THROW(Dune::Exception, "Type not recognised");
  }
};

template <>
struct TypeTraits<float> {
  static hid_t getType() { return H5T_NATIVE_FLOAT; }
};
template <>
struct TypeTraits<double> {
  static hid_t getType() { return H5T_NATIVE_DOUBLE; }
};
template <>
struct TypeTraits<long double> {
  static hid_t getType() { return H5T_NATIVE_LDOUBLE; }
};

template <>
struct TypeTraits<char> {
  static hid_t getType() { return H5T_NATIVE_CHAR; }
};
template <>
struct TypeTraits<signed char> {
  static hid_t getType() { return H5T_NATIVE_SCHAR; }
};
template <>
struct TypeTraits<unsigned char> {
  static hid_t getType() { return H5T_NATIVE_UCHAR; }
};

template <>
struct TypeTraits<short> {
  static hid_t getType() { return H5T_NATIVE_SHORT; }
};
template <>
struct TypeTraits<unsigned short> {
  static hid_t getType() { return H5T_NATIVE_USHORT; }
};

template <>
struct TypeTraits<int> {
  static hid_t getType() { return H5T_NATIVE_INT; }
};
template <>
struct TypeTraits<unsigned int> {
  static hid_t getType() { return H5T_NATIVE_UINT; }
};

template <>
struct TypeTraits<long> {
  static hid_t getType() { return H5T_NATIVE_LONG; }
};
template <>
struct TypeTraits<unsigned long> {
  static hid_t getType() { return H5T_NATIVE_ULONG; }
};

template <>
struct TypeTraits<long long> {
  static hid_t getType() { return H5T_NATIVE_LLONG; }
};
template <>
struct TypeTraits<unsigned long long> {
  static hid_t getType() { return H5T_NATIVE_ULLONG; }
};
}
#endif
