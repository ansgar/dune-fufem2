#ifndef TETRA_TENSOR_HH
#define TETRA_TENSOR_HH

#include "elasticitytensor.hh"

/** \brief class representing elasticity tensors for materials with tetragonal crystal symmetry in Voigt notation
 *
 */

template <int dim>
class TetraTensor;

template <>
class TetraTensor<3> : public ElasticityTensor<3>
{
    public:
        /**  \brief constructor for elasticity tensors with tetragonal symmetry
          *
          *  Parameters are named according to Voigt notation
          */
        TetraTensor(double C11, double C12, double C13, double C33, double C44, double C66)
        {
            ElasticityTensor<3>::operator=(0.0);

            (*this)[0][0] = C11;
            (*this)[1][1] = C11;
            (*this)[2][2] = C33;
            (*this)[5][5] = 2*C44;
            (*this)[4][4] = 2*C44;
            (*this)[3][3] = 2*C66;

            (*this)[0][1] = C12;
            (*this)[0][2] = C13;
            (*this)[1][0] = C12;
            (*this)[1][2] = C13;
            (*this)[2][0] = C13;
            (*this)[2][1] = C13;
        }
};

template <>
class TetraTensor<2> : public ElasticityTensor<2>
{
    public:
        /**  \brief for computations in 2 dimensions - the tetragonal symmetry can of course not be represented in 2D
          *
          *  Parameters are named according to Voigt notation
          */
        TetraTensor(double C11, double C22, double C33, double C12)
        {
            ElasticityTensor<2>::operator=(0.0);

            (*this)[0][0] = C11;
            (*this)[1][1] = C22;
            (*this)[2][2] = 2*C33;

            (*this)[0][1] = C12;
            (*this)[1][0] = C12;
        }
};

#endif
