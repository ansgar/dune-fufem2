// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_CALLABLE_HH
#define DUNE_FUFEM_PYTHON_CALLABLE_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <Python.h>

#include <string>
#include <sstream>
#include <vector>
#include <map>

#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/indices.hh>

#include <dune/grid/common/gridfactory.hh>

#include <dune/fufem/formatstring.hh>


#include <dune/fufem/python/reference.hh>


namespace Python
{

// forward declarations
void handlePythonError(const std::string&, const std::string&);
Reference dict();

namespace Imp
{
    // forward declarations
    PyObject* inc(PyObject* p);



    // Helper for marking a keyword argument
    template<class Key, class Value>
    struct KeyWordArgument
    {
        Key key;
        Value value;
    };



    // Check for KeyWordArgument
    template<class T>
    struct IsKeyWordArgument : std::false_type {};

    template<class Key, class Value>
    struct IsKeyWordArgument<KeyWordArgument<Key, Value>> : std::true_type {};

    // Count positional (non-keyword) arguments up to pos-1.
    // The result is returned as compile time constant using index_constant.
    template<std::size_t pos, class... Args>
    auto positionalArgumentCount()
    {
        using ArgTuple = std::tuple<Args...>;
        return Dune::unpackIntegerSequence([] (auto... i) {
            return Dune::index_constant<(0 + ... + not(Imp::IsKeyWordArgument<std::decay_t<std::tuple_element_t<i, ArgTuple>>>::value))>();
        }, std::make_index_sequence<pos>{});
    }

    // Helper for tagging string as keyword
    template<class Char>
    struct KeyWord {
        std::string key;
        template <typename Value>
        KeyWordArgument<std::string, Value> operator=(Value&& value) const {
            return {key, std::forward<Value>(value)};
        }
    };

    // Parse argument list. The list may contain positional and keyword arguments.
    // The parameters to this function are two callback functions, followed by
    // the arguments to be parsed. Positional and keyword arguments may be interleaved.
    // In any case only positional arguments are counted when determining the position
    // of a positional argument.
    //
    // The first callback will be called with (pos,arg) for each positional argument arg
    // with its position pos (as compile time value using Dune::index_constant).
    // The second callback will be called with (keyword, value) for each
    // keyword argument.
    template<class PositionalCallback, class KeyWordCallBack, class... Args>
    void parseKeywordArguments(PositionalCallback&& posCallback, KeyWordCallBack&& kwCallBack, Args&&... args)
    {
        auto processArgument = [&](auto i, auto&& arg)
        {
            if constexpr (IsKeyWordArgument<std::decay_t<decltype(arg)>>::value)
                kwCallBack(arg.key, arg.value);
            else
                posCallback(positionalArgumentCount<i, Args...>(), std::forward<decltype(arg)>(arg));
        };
        // Unfold argument pack to process arguments. Doing this via
        // std::initializer_list guarantees the proper evaluation order.
        // To additionally pass the argument indices as variadic argument pack,
        // we use unpackIntegerSequence.
        //
        // Here one would like to simply unpack args like this:
        // std::initializer_list<int>{ (processArgument(i, args), 0)...};
        // But capturing parameter packs does not work in C++17
        // (in fact gcc allows it). Hence we have to artificially
        // wrap in this in a tuple and access entries using std::get.
        //
        // Create a tuple preserving value categories.
        // This will store appropriate l- and r-value references
        // to the individual arguments.
        auto argTuple = std::forward_as_tuple(std::forward<Args>(args)...);
        Dune::unpackIntegerSequence([&](auto... i) {
            // Extract tuple entries restoring the value category.
            // Notice that due to reference collapsing this also returns
            // l-value references properly. Furthermore doing multiple
            // move's on the same tuple is OK here, because each move
            // only changes the value category of the returned entry
            // but does not modify the tuple itself.
            std::initializer_list<int>{ (processArgument(i, std::get<i>(std::move(argTuple))), 0)...};
        }, std::index_sequence_for<Args...>());
    }

} // namespace Imp


/**
 * \brief Create a keyword argument
 *
 * While the value may be stored as reference or
 * value, if an l-value or r-value reference
 * is passed, the key is always stored by value.
 * As a special case, when passing a
 * const char* it will be converted to a std::string.
 *
 * \param key Identifier of the argument
 * \param value Value of the argument
 */
template<class Key, class Value>
auto arg(Key key, Value&& value)
{
    using StoredKey = std::conditional_t<std::is_same_v<Key, const char*>, std::string, Key>;
    return Imp::KeyWordArgument<StoredKey, Value>{key, std::forward<Value>(value)};
}

namespace Literals {
    /**
     * \brief User defined literal for defining string-based keyword arguments
     *
     * This allows to create keyword arguments using "keyword"_a=value.
     */
    Imp::KeyWord<char> operator "" _a(const char* keyword, std::size_t) {
        return {keyword};
    }
}



/**
 * \brief Wrapper class for callable python objects
 *
 * This class derives from Python::Reference and
 * encapsulates functionality of callable objects.
 */
class Callable :
    public Reference
{
    public:

        /**
         * \brief Construct empty Callable
         */
        Callable() :
            Reference()
        {}

        /**
         * \brief Construct Callable from PyObject*
         *
         * Only to be used if you want to extend the interface
         * using the python api.
         *
         * This forwards to the corresponding constructor of Reference
         * and then checks if the python object is callable. If this is
         * not the case an exception is thrown. As a consequence the
         * reference count of the PyObject will be correctly decreased by
         * ~Reference even if the exception is thrown.
         *
         * But be carefull to always increment the count of a borrowed
         * reference BEFORE calling this constructor. I.e. always use
         *
         *   Callable(Imp::inc(p))
         *
         * instead of
         *
         *   Imp::inc(Callable(p))
         *
         * The latter may throw an exception and then decrease the count
         * before it is increased.
         *
         */
        Callable(PyObject* p) :
            Reference(p)
        {
            assertCallable(p_, "Callable(PyObject*)");
        }

        /**
         * \brief Construct Callable from Reference
         *
         * This checks if the python object is callable. If this is
         * not the case an exception is thrown.
         *
         * For implementors:
         *
         * This will increment the count for the
         * stored reference.
         */
        Callable(const Reference& other) :
            Reference(other)
        {
            assertCallable(p_, "Callable(Reference&)");
        }

        /**
         * \brief Destructor
         */
        virtual ~Callable()
        {}

        /**
         * \brief Assignment
         *
         * This will check if the python object is callable.
         * If this is not the case an exception is thrown.
         */
        virtual Callable& operator= (const Reference& other)
        {
            assertCallable(other, "Callable::operator=(Reference&)");
            Reference::operator=(other);
            return *this;
        }

        /**
         * \brief Call this Reference with arguments given as tuple
         * 
         * If the Reference represents a function it's called.
         * If the Reference represents an instance its __call__ method is invoked.
         * If the Reference represents a class a constructor is invoked.
         * In any case the arguments are given as a tuple as obtained by the
         * global method Python::makeTuple().
         *
         * Although the method is const it might change the refered object!
         * This cannot be avoided since the python api does not know
         * about const pointers so we use a mutable pointer internally.
         *
         * \param args Arguments represented as tuple
         * \returns The result of the call
         *
         */
        Reference callWithArgumentTuple(const Reference& args) const
        {
            assertPyObject("Callable::callWithArgumentTuple()");
            PyObject* result = PyObject_CallObject(p_, args);
            if (not result)
                handlePythonError("Callable::callWithArgumentTuple()", "failed to call object");
            return result;
        }

        /**
         * \brief Call this Reference with positional arguments given as tuple and keyword arguments given as dictionary
         *
         * If the Reference represents a function it's called.
         * If the Reference represents an instance its __call__ method is invoked.
         * If the Reference represents a class a constructor is invoked.
         *
         * Although the method is const it might change the refered object!
         * This cannot be avoided since the python api does not know
         * about const pointers so we use a mutable pointer internally.
         *
         * \param args Positional arguments represented as tuple
         * \param keywordArgs Keyword arguments represented as dictionary
         * \returns The result of the call
         *
         */
        Reference callWithArgumentTupleAndKeywordArgs(const Reference& args, const Reference& keywordArgs) const
        {
            assertPyObject("Callable::callWithArgumentTupleAndKeywordArgs()");
            PyObject* result = PyObject_Call(p_, args, keywordArgs);
            if (not result)
                handlePythonError("Callable::callWithArgumentTupleAndKeywordArgs()", "failed to call object");
            return result;
        }

        /**
         * \brief Call this object with given arguments
         *
         * Convert given arguments to Python-types and pass them
         * as arguments to Python-callable objects. Keyword arguments
         * can be passed using either Python::arg("keyword", value)
         * or "keyword"_a = value. For the latter you have to import
         * the namespace Python::Literals.
         *
         * \returns Result of the call as Python object.
         */
        template<class... Args>
        Reference operator() (const Args&... args) const
        {
            static constexpr std::size_t kwArgCount = (0 + ... + Imp::IsKeyWordArgument<Args>::value);
            if constexpr (kwArgCount == 0)
                return callWithArgumentTuple(makeTuple(args...));
            else
            {
                Reference kwArgDict = dict();
                Reference argTuple = PyTuple_New(sizeof...(Args)-kwArgCount);
                Imp::parseKeywordArguments(
                    [&](auto position, auto&& arg) {
                        PyTuple_SetItem(argTuple, position, Imp::inc(makeObject(arg)));
                    }, [&](auto keyword, auto&& value) {
                        PyDict_SetItem(kwArgDict, makeObject(keyword), makeObject(value));
                        // We have to use inc() since PyTuple_SetItem STEALS a reference!
                    }, args...);
                return callWithArgumentTupleAndKeywordArgs(argTuple, kwArgDict);
            }
        }

    protected:

        /**
         * \brief Assert that internal PyObject* is not NULL and callable and raise exception otherwise
         *
         * \param origin A string describing the origin of the error
         */
        static void assertCallable(PyObject* p, const std::string& origin)
        {
            if (p and (not PyCallable_Check(p)))
                DUNE_THROW(Dune::Exception,
                    "Python error occured." << std::endl <<
                    "  Origin:  " << origin << std::endl <<
                    "  Error:   Trying to use a non-callable as callable");
        }
};


/**
 * \brief Convert Python::Callable to C-function object
 *
 * \tparam ResultType C++-type of result
 *
 * This will convert the callable python object to
 * a function object with desired result type. The
 * difference between the passed and returned function
 * object is, that the latter converts return values
 * to the desired C++ type whereas the former returns
 * a Python::Reference.
 */
template<class ResultType>
auto make_function(Callable pyCallable)
{
    return [pyCallable](auto&&... args) {
        return pyCallable(std::forward<decltype(args)>(args)...).template toC<ResultType>();
    };
}

} // end of namespace Python



#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // DUNE_FUFEM_PYTHON_CALLABLE_HH


#endif

