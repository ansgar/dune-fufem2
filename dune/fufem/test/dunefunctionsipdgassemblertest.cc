// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#include <config.h>

#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/common/fmatrix.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/io.hh>

#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/istlbackend.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

// Include vintage assemblers for comparision
#include <dune/fufem/assemblers/intersectionoperatorassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>

#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/interiorpenaltydgassembler.hh>


#include <dune/functions/functionspacebases/lagrangedgbasis.hh>

/** Test whether the Dune::Fufem::DuneFunctionsOperatorAssembler and the classical Fufem assemblers
 *  assemble indeed the same matrix for the IPDG terms
 */
int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

//  const int dim = 2;
//  const int refine = 9;

//  const int dim = 2;
//  const int refine = 4;

  const int dim = 2;
  const int refine = 1;
  constexpr int p = 2; // piecewise quadratic basis functions

  // Build a test grid
  typedef Dune::YaspGrid<dim> GridType;

  Dune::FieldVector<double,dim> h(1);
  std::array<int,dim> n;
  n.fill(2);
  n[0] = 3;

  GridType grid(h,n);
  grid.globalRefine(refine);

//  static const std::size_t N = 4;
  static const std::size_t N = 1;

  using Basis = Dune::Functions::LagrangeDGBasis<GridType::LeafGridView, p>;
  Basis basis(grid.leafGridView());

  using Field = double;
  using Basis = decltype(basis);

  using LocalMatrix =  Dune::FieldMatrix<Field,N,N>;
  using Matrix = Dune::BCRSMatrix<LocalMatrix>;

  using Assembler = Dune::Fufem::DuneFunctionsOperatorAssembler<Basis, Basis>;

  auto matrix = Matrix{};

  auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
  auto patternBuilder = matrixBackend.patternBuilder();

  auto assembler = Assembler{basis, basis};

  assembler.assembleSkeletonPattern(patternBuilder);

  patternBuilder.setupMatrix();

  using FiniteElement = std::decay_t<decltype(basis.localView().tree().finiteElement())>;

  auto vintageIPDGAssembler = InteriorPenaltyDGAssembler<GridType, FiniteElement, FiniteElement>();
  auto localBlockAssembler = [&](const auto& edge, auto& matrixContainer,
      auto&& insideTrialLocalView, auto&& insideAnsatzLocalView, auto&& outsideTrialLocalView, auto&& outsideAnsatzLocalView)
  {
      vintageIPDGAssembler.assembleBlockwise(edge, matrixContainer, insideTrialLocalView.tree().finiteElement(),
                                             insideAnsatzLocalView.tree().finiteElement(),
                                             outsideTrialLocalView.tree().finiteElement(),
                                             outsideAnsatzLocalView.tree().finiteElement());
  };
  auto localBoundaryAssembler = [&](const auto& edge, auto& localMatrix, auto&& insideTrialLocalView, auto&& insideAnsatzLocalView)
  {
      vintageIPDGAssembler.assemble(edge, localMatrix, insideTrialLocalView.tree().finiteElement(), insideAnsatzLocalView.tree().finiteElement());
  };

  assembler.assembleSkeletonEntries(matrixBackend, localBlockAssembler, localBoundaryAssembler); // IPDG terms

  /* Now assemble in the classical fufem way and compare */
  auto fufemMatrix = Matrix{};
  // Wrap into fufem format
  using FufemBasis = DuneFunctionsBasis<Basis>;
  auto fufemBasis = FufemBasis(basis);
  // set up fufem Assemblers
  using FufemIntersectionOperatorAssembler = IntersectionOperatorAssembler<FufemBasis, FufemBasis>;
  using FufemIPDGAssembler = InteriorPenaltyDGAssembler<GridType, FufemBasis::LocalFiniteElement, FufemBasis::LocalFiniteElement>;

  auto isAssembler = FufemIntersectionOperatorAssembler{fufemBasis, fufemBasis};
  auto fufemIPDGAssembler = FufemIPDGAssembler();

  // Assemble fufem style
  isAssembler.assemble(fufemIPDGAssembler, fufemMatrix);

  // Compare:
  fufemMatrix-=matrix;
  const double error = fufemMatrix.infinity_norm();
  std:: cout << "Difference between old fufem assemblers and new dune-functions assembler: " << error << std::endl;

  // Errors should (if at all) only accrue from small numerical instablities, hence the very strict treshold
  return (error<1e-15) ? 0 : 1;
}

