#include <config.h>

#include <dune/common/test/iteratortest.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/io.hh>

#include <dune/fufem/mappedmatrix.hh>

using namespace Dune;

/** \brief Turns each entry of a scalar matrix into a 4x4 diagonal matrix
 *
 * The resulting 4x4 matrices are implemented as 2x2 block matrices of size 2x2 each.
 */
template <typename OriginalMatrix>
class DiagonalBlowUpMap
{
public:
  static constexpr int rowFactor = 2; // number of rows will be this times larger
  static constexpr int colFactor = 2; // number of columns will be this times larger

  using Matrix = OriginalMatrix; // type of matrix to be transformed

  using block_type = FieldMatrix<double,2,2>; // type of entries for transformed matrix

  // entry transformation
  // transform (virtualRow,virtualCol)-th copy of (row,col)-th entry A original matrix
  block_type apply(const typename Matrix::block_type& A, int row, int col, int virtualRow, int virtualCol) const
  {
    block_type result(0);

    if (virtualRow!=virtualCol)
      return result;

    for (int i=0; i<2; i++)
      result[i][i] = A;

    return result;
  }
};

int main (int argc, char *argv[])
{
  bool passed = true;

  // Build a test matrix
  BCRSMatrix<double> matrix(4, 4,
                            2,    // Expected average number of nonzeros
                                  // per row
                            0.2,  // Size of the compression buffer,
                                  // as a fraction of the expected
                                  // total number of nonzeros
                            BCRSMatrix<double>::implicit);

  matrix.entry(0,1) = 3;
  matrix.entry(1,0) = 4;
  matrix.entry(1,1) = 1;
  matrix.entry(2,1) = 5;
  matrix.entry(2,2) = 9;
  matrix.entry(2,3) = 2;
  matrix.entry(3,0) = 6;
  matrix.entry(3,3) = 5;

  matrix.compress();

  // Construct a mapped matrix for testing
  DiagonalBlowUpMap<decltype(matrix)> diagonalBlowUpMap;
  MappedMatrix mappedMatrix(diagonalBlowUpMap, matrix);

  // Test whether printmatrix compiles and does not crash
  printmatrix(std::cout, mappedMatrix, "mapped matrix", "--");

  // Test iteration over rows
  auto opt = [](auto&& row) { [[maybe_unused]] auto foo = row.size(); };
  passed = passed && !testRandomAccessIterator(mappedMatrix.begin(), mappedMatrix.end(), opt);

  // Can I do range-based for over the rows and entries?
  size_t blockEntryCounter = 0;

  for (auto&& row : mappedMatrix)
  {
    for ([[maybe_unused]] auto&& [entryI,   i] : sparseRange(row))
    {
      blockEntryCounter++;
    }
  }

  if (blockEntryCounter != 4*matrix.nonzeroes())
  {
    std::cerr << "Block count yielded incorrect result: "
              << blockEntryCounter << " instead of " << 4*matrix.nonzeroes() << std::endl;
    passed = false;
  }

  return passed ? 0 : 1;
}
