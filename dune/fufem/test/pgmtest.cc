#include <config.h>

#include <cstdio>
#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/portablegreymap.hh>


// This test tests the PortableGreyMap class by
// * creating a GridFunction and exporting it to a pgm-file
// * reading the above pgm-file into a PortableGreyMap-Object (internally a GridFunction)
// * checking the two for equal coefficient vectors (this can only work for grids with 2^n+1
//   nodes in each dimension, because when reading a pgm, for efficiency reasons, the underlying 
//   grid will have such a resolution)
//
// This is tested on YaspGrid only as
// * it is included in the dune-grid core module
// * PortableGreyMap uses a YaspGrid internally. Hence comparison is made easy.
//
// This test does NOT test all of PortableGreyMap's functionalities. Actually it doesn't test any.
// Only consistency of the read and export routines in the default colorscheme is tested.


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    typedef Dune::YaspGrid<2> GridType;
    typedef Dune::FieldVector<double, 1> RangeType;
    typedef Q1NodalBasis<GridType::LeafGridView> BasisType;
    typedef BasisGridFunction<BasisType, Dune::BlockVector<RangeType> > FunctionType;

    int n = 16;

    Dune::FieldVector<double,2> L(n-1);
    std::array<int,2> s = {{n-1, n-1}};

    GridType grid(L,s);
    BasisType basis(grid.leafGridView());

    /* create GridFunction */
    Dune::BlockVector<RangeType> disc_function_ori(basis.size());

    for (size_t i=0; i < disc_function_ori.size(); ++i)
        disc_function_ori[i] = i;

    FunctionType function(basis, disc_function_ori);

    /* export GridFunction to pgm */
    std::string filename("pgmtest.pgm");
    PortableGreyMap::exportGreyMap(filename.c_str(), function, 0, basis.size()-1, 0, L[0], 0, L[1], n, n, "pgmtest", 65535, PortableGreyMap::DEFAULT);

    /* read pgm to PortableGreyMap */
    PortableGreyMap pgm(0,basis.size()-1,PortableGreyMap::DEFAULT);

    pgm.readGreyMap(filename.c_str());

    /* translate PortableGreyMap to actual function */
    Dune::BlockVector<RangeType> disc_function_recon(basis.size());

    typedef GridType::LeafGridView::Codim<GridType::dimension>::Iterator NodeIterator;
    NodeIterator node = grid.leafGridView().begin<GridType::dimension>();
    NodeIterator node_end = grid.leafGridView().end<GridType::dimension>();

    for (; node != node_end; ++node)
         pgm.evaluate(node->geometry().corner(0),disc_function_recon[grid.leafGridView().indexSet().index<GridType::dimension>(*node)]);

    /* check for consistency */
    for (size_t i = 0; i<disc_function_ori.size(); ++i)
        if (disc_function_ori[i] != std::floor(disc_function_recon[i]+0.5))
        {
            std::cout << "original:\n" << disc_function_ori << std::endl;
            std::cout << "reconstructed:\n" << disc_function_recon << std::endl;
            remove(filename.c_str());
            DUNE_THROW(Dune::Exception, "Original and reconstructed function values don't match");
        }

    /* delete the PGM-file again */
    remove(filename.c_str());

}
