#include <config.h>

#include <cmath>
#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/function.hh>

#include <dune/fufem/functions/sumfunction.hh>
#include <dune/fufem/functions/sumgridfunction.hh>
#include <dune/fufem/functions/constantfunction.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include "common.hh"

/** \brief sin^2(comp) or cos^2(comp) as specified in constructor
 *
 */
template <class DT, class RT>
class TrigFunctionSquared:
    public Dune::VirtualFunction<DT,RT>
{
        typedef Dune::VirtualFunction<DT,RT> BaseType;

    public:
        enum Trig {
            SIN,
            COS
        };

        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;

        TrigFunctionSquared(int comp, Trig basic_trig):
            trig_(basic_trig),
            comp_(comp)
        {}

        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            double temp{0};
            switch (trig_) {
                case SIN:
                    temp=sin(x[comp_]);
                    break;
                case COS:
                    temp=cos(x[comp_]);
                    break;
            }
            y = temp*temp;
        }
    private:
        Trig trig_;
        int comp_;
};

/** \brief TestSuite for SumFunction
 *
 *  Takes a '2D partition of 1', here 1 = (0.5*sin^2(x)+0.5*sin^2(y)) + (0.5*cos^2(x)+0.5*cos^2(y)), and implement it as nested SumFunction.
 *  Validation against 1.
 */
struct SumFunctionTestSuite
{
    bool check()
    {
        const int DDIM=2;
        const int RDIM=2;

        const int n_testpoints=100;

        typedef Dune::FieldVector<double,DDIM> DomainType;
        typedef Dune::FieldVector<double,RDIM> RangeType;

        typedef TrigFunctionSquared<DomainType,RangeType> TFType;

        TFType sin2x(0,TFType::SIN);
        TFType sin2y(1,TFType::SIN);
        TFType cos2x(0,TFType::COS);
        TFType cos2y(1,TFType::COS);

        SumFunction<DomainType,RangeType> sumfunction1;
        sumfunction1.registerFunction(0.5, sin2x);
        sumfunction1.registerFunction(0.5, sin2y);
        SumFunction<DomainType,RangeType> sumfunction2;
        sumfunction2.registerFunction(0.5, cos2x);
        sumfunction2.registerFunction(0.5, cos2y);
        SumFunction<DomainType,RangeType> sumfunction;
        sumfunction.registerFunction(1.0,sumfunction1);
        sumfunction.registerFunction(1.0,sumfunction2);

        SumFunction<DomainType,RangeType> checkfunction;
        checkfunction.registerFunction(1.0,sin2x);
        checkfunction.registerFunction(1.0,cos2x);

        DomainType x(0.0);
        RangeType  y(0.0),
                   ycheck(0.0);
        for (int run=0; run<n_testpoints; ++run)
        {
            for (int dcomp=0; dcomp<DDIM; ++dcomp)
                x[dcomp] = (6.3*rand())/RAND_MAX - 3.15;

            sumfunction.evaluate(x,y);
            checkfunction.evaluate(x,ycheck);

            for (int rcomp=0; rcomp<RDIM; ++rcomp)
                if (y[rcomp]-1.0>1e-12)
                {
                    std::cout << "(0.5*sin^2(x)+0.5*sin^2(y)) + (0.5*cos^2(x)+0.5*cos^2(y)) = " << y[rcomp] << std::endl;
                    std::cout << "check: sin^2(x)+cos^2(x) = " << ycheck[rcomp] << std::endl;

                    return false;
                }
        }

        return true;
    }
};

/** \brief TestSuite for SumGridFunction
 *
 *  Use partition of 1 as above for P1 GridFunctions. They still should add up to 1 everywhere.
 *
 */
struct SumGridFunctionTestSuite
{
    template <class GridType>
    bool check(GridType& grid)
    {
        const int RDIM=2;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate DomainType;
        typedef Dune::FieldVector<double,RDIM> RangeType;

        typedef TrigFunctionSquared<DomainType,RangeType> TFType;

        TFType sin2x(0,TFType::SIN);
        TFType sin2y(1,TFType::SIN);
        TFType cos2x(0,TFType::COS);
        TFType cos2y(1,TFType::COS);

        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<typename GridType::LeafGridView,1 > > Basis;
        Basis basis(grid.leafGridView());

        typedef Dune::BlockVector<RangeType> CoeffType;
        CoeffType coeffs1(basis.size()),
                  coeffs2(basis.size()),
                  coeffs3(basis.size()),
                  coeffs4(basis.size());

        typedef BasisGridFunction<Basis,CoeffType> GridFunctionType;

        CoeffType sumVector;

        Functions::interpolate(basis,coeffs1,sin2x);
        GridFunctionType summand1(basis,coeffs1);
        sumVector = coeffs1;
        Functions::interpolate(basis,coeffs2,sin2y);
        GridFunctionType summand2(basis,coeffs2);
        sumVector += coeffs2;
        Functions::interpolate(basis,coeffs3,cos2x);
        GridFunctionType summand3(basis,coeffs3);
        sumVector += coeffs3;
        Functions::interpolate(basis,coeffs4,cos2y);
        GridFunctionType summand4(basis,coeffs4);
        sumVector += coeffs4;

        for (size_t i=0; i<basis.size();++i)
            for (int j=0; j<RDIM; ++j)
                if (std::abs(sumVector[i][j]-2.0) > 1e-12 )
                {
                    std::cout << i << ", " << j << ": " << sumVector[i][j] << std::endl;
                    return false;
                }

        SumGridFunction<GridType,RangeType> sumfunction1(grid);
        sumfunction1.registerFunction(0.5, summand1);
        sumfunction1.registerFunction(0.5, summand2);
        SumGridFunction<GridType,RangeType> sumfunction2(grid);
        sumfunction2.registerFunction(0.5, summand3);
        sumfunction2.registerFunction(0.5, summand4);
        SumGridFunction<GridType,RangeType> sumfunction(grid);
        sumfunction.registerFunction(1.0,sumfunction1);
        sumfunction.registerFunction(1.0,sumfunction2);

        SumGridFunction<GridType,RangeType> checkfunction(grid);
        checkfunction.registerFunction(1.0, summand1);
        checkfunction.registerFunction(1.0, summand3);

        ConstantFunction<DomainType,RangeType> one(RangeType(1.0));

        return compareEvaluateByGridViewQuadrature(sumfunction, one, grid.leafGridView(), 3);
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the SumFunctionTest" << std::endl;

    SumFunctionTestSuite sftests;
    SumGridFunctionTestSuite sgftests;

    bool passed = sftests.check();
    if (passed)
        std::cout << "SumFunction test passed" << std::endl;
    else
        std::cout << "SumFunction test failed" << std::endl;

#if HAVE_DUNE_UGGRID
    passed = passed and checkWithAdaptiveGrid<Dune::UGGrid<2> >(sgftests, 3, 3);
    if (not passed)
        std::cout << "SumGridFunction test failed on locally refined UGGrid<2>." << std::endl;
#if HAVE_DUNE_SUBGRID
    passed = passed and checkWithSubGrid<Dune::UGGrid<2> >(sgftests, 3, 3, "SubGrid< 2,UGGrid<2> >");
    if (not passed)
        std::cout << "SumGridFunction test failed on locally refined SubGrid< 2,UGGrid<2> >." << std::endl;
#endif
#endif

#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithAdaptiveGrid<Dune::ALUGrid<2,2, Dune::simplex, Dune::nonconforming> >(sgftests, 3, 3);
    if (not passed)
        std::cout << "SumGridFunction test failed on locally refined ALUGrid<2,2,simplex,nonconforming>." << std::endl;
#if HAVE_DUNE_SUBGRID
    passed = passed and checkWithSubGrid<Dune::ALUGrid<2,2, Dune::simplex, Dune::nonconforming> >(sgftests, 3, 3, "SubGrid< 3,ALUGrid<...> >");
    if (not passed)
        std::cout << "SumGridFunction test failed on locally refined SubGrid< 2,ALUGrid<2,2,simplex,nonconforming> >." << std::endl;
#endif
#endif


    return passed ? 0 : 1;

}
