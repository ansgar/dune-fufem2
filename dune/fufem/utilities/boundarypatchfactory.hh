// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_FUFEM_UTILITIES_BOUNDARYPATCH_FACTORY_HH
#define DUNE_FUFEM_UTILITIES_BOUNDARYPATCH_FACTORY_HH

#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/readbitfield.hh>

//! Extract faces where multiple criterions are met which are given by lambdas
template <class GridView>
struct CombinedFaceInsertion {

  using It = typename GridView::Intersection;

  CombinedFaceInsertion() = default;

  //! check if intersection should be inserted into the patch
  bool operator() (const It& it) const {

    for (size_t i=0; i<criteria_.size(); i++)
      if (!criteria_[i](it))
        return false;

    return true;
  }

  //! Add insertion criterion
  void add(std::function<bool(const It&)> insertion) {
    criteria_.push_back(insertion);
  }

private:
  std::vector<std::function<bool(const It&)> > criteria_;
};

/** \brief Setup a boundary patch from criterions given in a parameter file. */
template <class GridView>
class BoundaryPatchFactory
{
public:

  /** \brief Setup boundary patch using criterions prescribed in the parameter file.
   *
   * Use the following data in your parameter file
   *
   * nCriterions - The number of criterions that you want to use
   * for each criterion a [sub] called [criterion0], [criterion1], ...
   * the [criterionK] must contain a
   *    type : LowerCenter  (insert faces for which a component of the centre is below some bound
   *           UpperCenter  (insert faces for which a component of the centre is above some bound
   *           LowerNormal  (insert faces for which a component of the normal is below some bound
   *           UpperNormal  (insert faces for which a component of the normal is above some bound
   * and the needed members
   *    component   - the coordinate component which should be bounded
   *    bound       - the upper/lower bound
   *
   * \param[in] config  The parameter file
   * \param[in] patch The boundarypatch to be setup
   * \param[out] true if the boundary patch could be setup
   */
  static bool setupBoundaryPatch(const Dune::ParameterTree& config, BoundaryPatch<GridView>& patch, std::string path = "") {

    // if an amira format file is found then read it
    if (config.hasKey("amiraPatchFile")) {
      readBoundaryPatch<typename GridView::Grid>(patch, path + config.get<std::string>("amiraPatchFile"));
      return true;
    }

    if (!config.hasKey("nCriterions"))
        return false;

    // setup patch from given criterions
    int nCriterions = config.get<int>("nCriterions");
    CombinedFaceInsertion<GridView> faceInsertion;

    using It = typename GridView::Intersection;
    Dune::FieldVector<typename GridView::ctype,GridView::dimension-1> zero(0);

    // return whether any criterion was found
    bool found(false);

    // parse each insertion criterion
    for (int i=0; i<nCriterions; i++) {

      const auto& sub = config.sub("criterion" + std::to_string(i));

      // so far each criterion has a component and a bound
      int comp   = sub.get<int>("component");
      auto bound = sub.get<typename GridView::ctype>("bound");
      auto type  = sub.get<std::string>("type");

      if (type=="LowerCentre")
        faceInsertion.add([=] (const It& it) {return it.geometry().center()[comp]<=bound;});
      else if (type=="UpperCentre")
        faceInsertion.add([=] (const It& it) {return it.geometry().center()[comp]>=bound;});
      else if (type=="LowerNormal")
        faceInsertion.add([=] (const It& it)
        {return (!it.neighbor() && (it.unitOuterNormal(zero)[comp]<=bound));});
      else if (type=="UpperNormal")
        faceInsertion.add([=] (const It& it)
        {return (!it.neighbor() && (it.unitOuterNormal(zero)[comp]>=bound));});
      else
        DUNE_THROW(Dune::NotImplemented, "There is no insertion criterion "+type);

      found = true;
    }

    // insert all faces into the boundary patch
    patch.insertFacesByProperty(faceInsertion);
    return found;
  }
};

#endif
